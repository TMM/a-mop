/*
<--------------------------------------------------------------------------->
- Developer(s): Cakes
- Complete: 100%
<--------------------------------------------------------------------------->
*/
#include "ScriptPCH.h"
#include "Chat.h"
<<<<<<< .merge_file_a08136
#include "TicketMgr.h"
=======
#include "Chat.h"
>>>>>>> .merge_file_a13880
 
enum enus
{
	GOSSIP_GOODBYE = 5000,

	// NPC Texts - Next level player-friendlyness
    SERVICES_MAIN_TEXTS      = 13371338,
	TELEPORTATION_TEXTS      = 13371339,
    TRANSMOG_TEXTS           = 13371340,
    EVENT_TEXTS              = 13371341,

    // Amount of reputation needed for Exalted
    REPUTATION_REQUIRED      = 42000,

    // Teleportation Services
    TELEPORTATION_SERVICES   = 0,
    TELEPORT_MALLS           = 1,
    TELEPORT_GURUBASHI       = 2,
    TELEPORT_DUELZONE        = 3,
    
    // Special Teleports
    TELEPORT_VIPMALL         = 4,
    TELEPORT_GMISLAND        = 5,

    // Transmogrification Services
    TRANSMOG_SERVICES        = 6,
    TELEPORT_TRANSMOG        = 7,
    TRANSMOG_REPUTATION      = 8,

    // Pet Services
    TELEPORT_PETZONE         = 9,

    // Event Services
    SCHEDULED_EVENTS_LIST    = 10,
    ACTIVE_EVENTS_LIST       = 11,

    // GM Services
    GM_SERVICES              = 12,
    GM_LEARNALL_ARMOR        = 13,
    GM_LEARNALL_WEAPONS      = 14,

<<<<<<< .merge_file_a08136
    // FAQ INFORMATION
    INFORMATION_FAQ_SERVICE  = 15,
    INFORMATION_FAQ_GENERAL  = 16,
    INFORMATION_FAQ_DONATION = 17,
    INFORMATION_FAQ_BUGS     = 18,
=======
       // GM-Only => Event System Integration
       // HELLO WORLD, CAKES
       // 
       // Tele to VIP
       // Tele to GM Island
       // Learn all armor
       // Learn all weps
       // Organise an Event/Manage 

                // ==> Organise an Event Menu Structure
                // ===========================
                // - Click here to select the event type
                // - Click here to select entry requirements
                // - Click here to schedule the time until the event starts, in minutes
                // - Click here to allow spectation (Default: Disallowed)
    ////
    // Organise an Event Menu
    ////
        EVENT_ORGANISE_MENU      = 23,
        EVENT_SELECT_TYPE        = 15, // menu
        EVENT_SELECT_ENTRY       = 16,
        EVENT_SELECT_TIMETIL     = 17,
        EVENT_SELECT_SPECTATE    = 18,

    // Event definitions
    // Select an Event Type
      // PVP Events
        EVENT_CHOOSE_1V1         = 25, // type 1
        EVENT_CHOOSE_2V2         = 26, // type 2
        EVENT_CHOOSE_3V3         = 27, // type 3
        EVENT_CHOOSE_4V4         = 28, // type 4
        EVENT_CHOOSE_5V5         = 29, // type 5
        EVENT_CHOOSE_10V10       = 30, // type 6
        EVENT_CHOOSE_20V20       = 31, // type 7
        EVENT_CHOOSE_FFA         = 33, // type 8  
      // Non PvP Events
        EVENT_CHOOSE_STAIRCASE   = 38, // type 9
        EVENT_CHOOSE_HNS         = 39, // type 10
        EVENT_CHOOSE_TRIV        = 36, // type 11
        EVENT_CHOOSE_FLYDIE      = 35, // type 12
        EVENT_CHOOSE_KILLGM      = 37, // type 13
        EVENT_CHOOSE_SCAV        = 32, // type 14
        EVENT_CHOOSE_TRANSMOG    = 34, // type 15

    // Entry Requirements for Gear
        EVENT_ENTRY_DREAD        = 40, // type 1
        EVENT_ENTRY_MALEV        = 41, // type 2
        EVENT_ENTRY_NOPVETR      = 42, // type 3
        EVENT_ENTRY_NONE         = 43, // type 4

    // Time until event definitions
        EVENT_TIME_20M           = 44,
        EVENT_TIME_30M           = 45,
        EVENT_TIME_40M           = 46,
        EVENT_TIME_50M           = 47,
        EVENT_TIME_1H            = 48,
        EVENT_TIME_1H30M         = 49,
        EVENT_TIME_2H            = 50,

    // Finally create the event
        EVENT_CREATE_FINAL_EVENT = 51,


    /////
    // Manage your event
    /////
    
                // ==> Manage an Event Menu Structure
                // ===========================
                // - [Hide and Seek] scheduled in X minutes (Dreadful Only)
                // - Click here to re-broadcast your event (2 minute cooldown)
                // - Click here to delay your event (cannot be delayed if <5 min)
                // - Click here to change entry requirements
                // - Start your event (finishes timer) 
                //      - Finish your event (if started)
               

        EVENT_MANAGE_MENU        = 52,
        EVENT_MANAGE_INFO        = 53,
        EVENT_MANAGE_REBROADCAST = 54,
        EVENT_MANAGE_DELAY       = 55,
        EVENT_MANAGE_ENTRYEDIT   = 56,
        EVENT_MANAGE_START       = 57,
        EVENT_MANAGE_FINISH      = 58,


    // FAQ INFORMATION
    INFORMATION_FAQ_SERVICE  = 19,
    INFORMATION_FAQ_GENERAL  = 20,
    INFORMATION_FAQ_DONATION = 21,
    INFORMATION_FAQ_BUGS     = 22,
>>>>>>> .merge_file_a13880

};

class global_teleporter : public CreatureScript
{
public:
    global_teleporter() : CreatureScript("global_teleporter") { }
 
    bool OnGossipHello(Player* pPlayer, Creature* pCreature)
    {
        if (pPlayer->isInCombat())
        {
            pPlayer->GetSession()->SendNotification("You are in combat");
            pPlayer->PlayerTalkClass->SendCloseGossip();
            return true;
        }

        // Greetings XX
        // {BOOK} GM Services

        // {BUBBLE} Teleportation Services
        // {BAG}    Transmogrification Services
        // {BOOK}   Pet Taming Zone
        // {CHAT}   Frequently Asked Questions
        // {BOOK}   Scheduled Events (2) <-- Blue --> Lists events inactive --> Displays info to signup
        // {BOOK}   Active Events (1) <-- Blue --> Displays info to spectate.

        // GM Services
        if (pPlayer->GetSession()->GetSecurity() > 2)
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "GM Services", GOSSIP_SENDER_MAIN, GM_SERVICES);

        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TAXI, "Teleportation Services", GOSSIP_SENDER_MAIN, TELEPORTATION_SERVICES);
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Transmogrification Services", GOSSIP_SENDER_MAIN, TRANSMOG_SERVICES);
        if (pPlayer->getClass() == CLASS_HUNTER)
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Pet Taming Zone", GOSSIP_SENDER_MAIN, TELEPORT_PETZONE);
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Assistance and Support (FAQ)", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_SERVICE);

<<<<<<< .merge_file_a08136
        // Get number of upcoming events
        QueryResult selectid = CharacterDatabase.PQuery("SELECT COUNT(*) FROM events WHERE `started` = '0' && `completed` = '0'");
        if (!selectid)
        {
            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Upcoming Scheduled Events (|cff0000ff0|r)", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);
        }
        else
        {
            Field * field = selectid->Fetch();
            uint32 numberscheduled = field[0].GetUInt32();

            std::stringstream buf;
            buf << "Upcoming Scheduled Events (|cff0000ff" << numberscheduled << "|r)";

            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, buf.str(), GOSSIP_SENDER_MAIN, SCHEDULED_EVENTS_LIST);
        }

        // Get number of active events
=======
         // Get number of active events
>>>>>>> .merge_file_a13880
        QueryResult selectid2 = CharacterDatabase.PQuery("SELECT COUNT(*) FROM events WHERE `started` = '1' && `completed` = '0'");
        if (!selectid2)
        {
            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Live Active Events (|cff0000ff0|r)", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);
        }
        else
        {
            Field * field = selectid2->Fetch();
            uint32 numberactive = field[0].GetUInt32();

            std::stringstream buf2;
            buf2 << "Live Active Events (|cff0000ff" << numberactive << "|r)";

            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, buf2.str(), GOSSIP_SENDER_MAIN, ACTIVE_EVENTS_LIST);
        }

<<<<<<< .merge_file_a08136
        
=======
        // Get number of upcoming events
        QueryResult selectid = CharacterDatabase.PQuery("SELECT COUNT(*) FROM events WHERE `started` = '0' && `completed` = '0' && `time` > 0");
        if (!selectid)
        {
            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Upcoming Scheduled Events (|cff0000ff0|r)", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);
        }
        else
        {
            Field * field = selectid->Fetch();
            uint32 numberscheduled = field[0].GetUInt32();

            std::stringstream buf;
            buf << "Upcoming Scheduled Events (|cff0000ff" << numberscheduled << "|r)";

            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, buf.str(), GOSSIP_SENDER_MAIN, SCHEDULED_EVENTS_LIST);
        }
>>>>>>> .merge_file_a13880

        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Close Services", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);
        pPlayer->PlayerTalkClass->SendGossipMenu(SERVICES_MAIN_TEXTS, pCreature->GetGUID());
        return true;
    }
 
    bool OnGossipSelect(Player* pPlayer, Creature* pCreature,uint32 uiSender, uint32 uiAction)
    {
        pPlayer->PlayerTalkClass->ClearMenus();
 
        switch (uiAction)
        {
            ////
            // GM Services
            ////

            case GM_SERVICES:
            {

                    // Welcome GM Dialogue
                    std::string GMName = pPlayer->GetSession()->GetPlayer()->GetName();
                    int GMlvl = pPlayer->GetSession()->GetSecurity();
                    std::stringstream buf;
                    buf << "Welcome to the GM Services Panel, |cff0000ff" << GMName << "|r (GM Rank: " << GMlvl <<")";
                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, buf.str(), GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);

                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TAXI, "Teleport to GM Island", GOSSIP_SENDER_MAIN, TELEPORT_GMISLAND);
                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TAXI, "Teleport to VIP Mall", GOSSIP_SENDER_MAIN, TELEPORT_VIPMALL);

                    // Learn weapon/armor proficiencies if the GM is new
                    if (!pPlayer->HasSpell(9116))
                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Learn all Armor Proficiencies", GOSSIP_SENDER_MAIN, GM_LEARNALL_ARMOR);
                    if (!pPlayer->HasSpell(266))
                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Learn all Weaponry Skills", GOSSIP_SENDER_MAIN, GM_LEARNALL_WEAPONS);

<<<<<<< .merge_file_a08136
=======
                    // Check if GM doesn't have an event already scheduled
                    QueryResult findevent = CharacterDatabase.PQuery("SELECT id FROM `events` WHERE `completed`='0' && `host` = '%s'", pPlayer->GetSession()->GetPlayer()->GetName());
                    if (!findevent)
                       pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Organise an Event", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU); 
                    else
                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Manage your current scheduled event", GOSSIP_SENDER_MAIN, EVENT_MANAGE_MENU); 
                    

>>>>>>> .merge_file_a13880
                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Goodbye...", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);
                    pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                    return true;
                    break;
            }

            case GM_LEARNALL_ARMOR:
            {
                // Cloth -> Plate Mail
                pPlayer->learnSpell(9078, false);
                pPlayer->learnSpell(8737, false);
                pPlayer->learnSpell(750, false);
                pPlayer->learnSpell(9116, false);

                ChatHandler(pPlayer->GetSession()).PSendSysMessage("All armor proficiencies have been taught! With great power comes great responsibility, young one...");
                pPlayer->PlayerTalkClass->SendCloseGossip();
                break;
            }

            case GM_LEARNALL_WEAPONS:
            {
                // All weapons
                pPlayer->learnSpell(264, false);
                pPlayer->learnSpell(5011, false);
                pPlayer->learnSpell(1180, false);
                pPlayer->learnSpell(204, false);
                pPlayer->learnSpell(81, false);
                pPlayer->learnSpell(674, false);
                pPlayer->learnSpell(15590, false);
                pPlayer->learnSpell(266, false);
                pPlayer->learnSpell(196, false);
                pPlayer->learnSpell(198, false);
                pPlayer->learnSpell(201, false);
                pPlayer->learnSpell(3127, false);
                pPlayer->learnSpell(200, false);
                pPlayer->learnSpell(3018, false);
                pPlayer->learnSpell(5019, false);
                pPlayer->learnSpell(227, false);
                pPlayer->learnSpell(2764, false);
                pPlayer->learnSpell(2567, false);
                pPlayer->learnSpell(197, false);
                pPlayer->learnSpell(199, false);
                pPlayer->learnSpell(202, false);
                pPlayer->learnSpell(203, false);
                pPlayer->learnSpell(5009, false);
                pPlayer->learnSpell(46917, false);

                ChatHandler(pPlayer->GetSession()).PSendSysMessage("All weaponry proficiencies have been taught! Make sure you read up on the event system guidebook to take full advantage of events!");
                pPlayer->PlayerTalkClass->SendCloseGossip();
                break;
            }

            ////
<<<<<<< .merge_file_a08136
=======
            // Event Organise Menu
            ////

            case EVENT_ORGANISE_MENU:
            {

                // No event scheduled yet options, that are incomplete
                QueryResult searchtype = CharacterDatabase.PQuery("SELECT eventtype FROM eventscache WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
                if (!searchtype)
                    {
                       pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Click here to select an event type", GOSSIP_SENDER_MAIN, EVENT_SELECT_TYPE);
                    }
                else
                {
                   Field * field = searchtype->Fetch();
                   uint32 eventtype = field[0].GetUInt32();

                switch (eventtype)
                {
                    case 1: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Current selected event: 1v1", GOSSIP_SENDER_MAIN, EVENT_SELECT_TYPE);
                        break;
                    case 2: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Current selected event: 2v2", GOSSIP_SENDER_MAIN, EVENT_SELECT_TYPE);
                        break;
                    case 3: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Current selected event: 3v3", GOSSIP_SENDER_MAIN, EVENT_SELECT_TYPE);
                        break;
                    case 4: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Current selected event: 4v4", GOSSIP_SENDER_MAIN, EVENT_SELECT_TYPE);
                        break;
                    case 5: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Current selected event: 5v5", GOSSIP_SENDER_MAIN, EVENT_SELECT_TYPE);
                        break;
                    case 6: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Current selected event: 10v10", GOSSIP_SENDER_MAIN, EVENT_SELECT_TYPE);
                        break;
                    case 7: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Current selected event: 20v20", GOSSIP_SENDER_MAIN, EVENT_SELECT_TYPE);
                        break;
                    case 8: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Current selected event: FFA", GOSSIP_SENDER_MAIN, EVENT_SELECT_TYPE);
                        break;
                    case 9: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Current selected event: Hide and Seek", GOSSIP_SENDER_MAIN, EVENT_SELECT_TYPE);
                        break;
                    case 10: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Current selected event: Trivia", GOSSIP_SENDER_MAIN, EVENT_SELECT_TYPE);
                        break;
                    case 11: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Current selected event: Fly or Die", GOSSIP_SENDER_MAIN, EVENT_SELECT_TYPE);
                        break;
                    case 12: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Current selected event: Scavenger Hunt", GOSSIP_SENDER_MAIN, EVENT_SELECT_TYPE);
                        break;
                    case 13: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Current selected event: Kill the GM", GOSSIP_SENDER_MAIN, EVENT_SELECT_TYPE);
                        break;
                    case 14: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Current selected event: Staircase Event", GOSSIP_SENDER_MAIN, EVENT_SELECT_TYPE);
                        break;
                    case 15: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Current selected event: Transmogrification Event", GOSSIP_SENDER_MAIN, EVENT_SELECT_TYPE);
                        break;

                default: break;
                }

                /*
                QueryResult searchreq = CharacterDatabase.PQuery("SELECT reqtype FROM eventscache WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
                if (!searchreq)
                    {
                       pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Click here to choose gear entry requirements", GOSSIP_SENDER_MAIN, EVENT_SELECT_ENTRY);
                    }
                else 
                    {
                       Field * field = searchreq->Fetch();
                       uint32 reqtype = field[0].GetUInt32();
                       switch (reqtype)
                           {
                            case 1: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Current entry requirement: Dreadful Only", GOSSIP_SENDER_MAIN, EVENT_SELECT_ENTRY);
                            break;
                            case 2: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Current entry requirement: Malevolent Only", GOSSIP_SENDER_MAIN, EVENT_SELECT_ENTRY);
                            break;
                            case 3: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Current entry requirement: No PvE Trinkets Only", GOSSIP_SENDER_MAIN, EVENT_SELECT_ENTRY);
                            break;
                            case 4: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "No entry requirements needed", GOSSIP_SENDER_MAIN, EVENT_SELECT_ENTRY);
                            break;
                                 default: break;
                           }
                    }
                    */

                
                QueryResult searchtime = CharacterDatabase.PQuery("SELECT timeleft FROM eventscache WHERE `gmid` = '%u' && `timeleft` > 0", pPlayer->GetSession()->GetGuidLow());
                if (!searchtime)
                    {
                       pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Click here to schedule the time until the event begins (minutes)", GOSSIP_SENDER_MAIN, EVENT_SELECT_TIMETIL);
                    }
                else 
                    {
                       Field * field = searchtime->Fetch();
                       uint32 time = field[0].GetUInt32();
                       switch (time)
                           {
                            case 20: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Selected time till event: 20 minutes", GOSSIP_SENDER_MAIN, EVENT_SELECT_TIMETIL);
                            break;
                            case 30: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Selected time till event: 30 minutes", GOSSIP_SENDER_MAIN, EVENT_SELECT_TIMETIL);
                            break;
                            case 40: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Selected time till event: 40 minutes", GOSSIP_SENDER_MAIN, EVENT_SELECT_TIMETIL);
                            break;
                            case 50: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Selected time till event: 50 minutes", GOSSIP_SENDER_MAIN, EVENT_SELECT_TIMETIL);
                            break;
                            case 60: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Selected time till event: 1 hour", GOSSIP_SENDER_MAIN, EVENT_SELECT_TIMETIL);
                            break;
                            case 90: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Selected time till event: 1 hour 30 minutes", GOSSIP_SENDER_MAIN, EVENT_SELECT_TIMETIL);
                            break;
                            case 120: pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Selected time till event: 2 hours", GOSSIP_SENDER_MAIN, EVENT_SELECT_TIMETIL);
                            break;
                                 default: break;
                           }
                    }
                // pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "(Locked) Click here to allow spectation (Default: Disallowed)", GOSSIP_SENDER_MAIN, EVENT_SELECT_SPECTATE);

                // Check if the values are in their default state
                if (eventtype > 0)
                // Function to actually create event //
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TRAINER, "Click here to confirm and schedule your event", GOSSIP_SENDER_MAIN, EVENT_CREATE_FINAL_EVENT, "Are you sure you want to schedule this?", 0, false);
                else
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You must choose your event details before confirming!", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);
                
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            }
            }
            
            case EVENT_CREATE_FINAL_EVENT:
            {
                QueryResult parsed = CharacterDatabase.PQuery("SELECT * FROM eventscache WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
                if (parsed)
                {
                       Field * field = parsed->Fetch();
                       uint32 gmid = field[0].GetUInt32();
                       uint32 eventtype = field[1].GetUInt32();
                       uint32 reqtype = field[2].GetUInt32();
                       int32 timeleft = field[3].GetUInt32();
                       int32 timeSecstoMin = timeleft * 60;

                       // 1v1
                       if (eventtype == 1)
                       {
                           char* eventname = "1v1 Tournament Event";
                           QueryResult confirm = CharacterDatabase.PQuery("INSERT INTO `events` (`eventname`,`time`,`host`,`reqtype`) VALUES ('%s','%u','%s','%u')", eventname, timeSecstoMin, pPlayer->GetSession()->GetPlayer()->GetName(), reqtype);
                       }
                       // 1v1
                       if (eventtype == 2)
                       {
                           char* eventname = "2v2 Tournament Event";
                           QueryResult confirm = CharacterDatabase.PQuery("INSERT INTO `events` (`eventname`,`time`,`host`,`reqtype`) VALUES ('%s','%u','%s','%u')", eventname, timeSecstoMin, pPlayer->GetSession()->GetPlayer()->GetName(), reqtype);  
                       }
                       // 3v3
                       if (eventtype == 3)
                       {
                           char* eventname = "3v3 Tournament Event";
                           QueryResult confirm = CharacterDatabase.PQuery("INSERT INTO `events` (`eventname`,`time`,`host`,`reqtype`) VALUES ('%s','%u','%s','%u')", eventname, timeSecstoMin, pPlayer->GetSession()->GetPlayer()->GetName(), reqtype);
                       } 
                       // 4v4
                       if (eventtype == 4)
                       {
                           char* eventname = "4v4 Tournament Event";
                           QueryResult confirm = CharacterDatabase.PQuery("INSERT INTO `events` (`eventname`,`time`,`host`,`reqtype`) VALUES ('%s','%u','%s','%u')", eventname, timeSecstoMin, pPlayer->GetSession()->GetPlayer()->GetName(), reqtype);
                       }
                       // 5v5
                       if (eventtype == 5)
                       {
                           char* eventname = "5v5 Tournament Event";
                           QueryResult confirm = CharacterDatabase.PQuery("INSERT INTO `events` (`eventname`,`time`,`host`,`reqtype`) VALUES ('%s','%u','%s','%u')", eventname, timeSecstoMin, pPlayer->GetSession()->GetPlayer()->GetName(), reqtype);
                       }
                       // 10v10
                       if (eventtype == 6)
                       {
                           char* eventname = "10v10 Tournament Event";
                           QueryResult confirm = CharacterDatabase.PQuery("INSERT INTO `events` (`eventname`,`time`,`host`,`reqtype`) VALUES ('%s','%u','%s','%u')", eventname, timeSecstoMin, pPlayer->GetSession()->GetPlayer()->GetName(), reqtype);
                       }
                       // 20v20
                       if (eventtype == 7)
                       {
                           char* eventname = "20v20 Tournament Event";
                           QueryResult confirm = CharacterDatabase.PQuery("INSERT INTO `events` (`eventname`,`time`,`host`,`reqtype`) VALUES ('%s','%u','%s','%u')", eventname, timeSecstoMin, pPlayer->GetSession()->GetPlayer()->GetName(), reqtype);
                       }
                       // FFA event
                       if (eventtype == 8)
                       {
                           char* eventname = "FFA Event";
                           QueryResult confirm = CharacterDatabase.PQuery("INSERT INTO `events` (`eventname`,`time`,`host`,`reqtype`) VALUES ('%s','%u','%s','%u')", eventname, timeSecstoMin, pPlayer->GetSession()->GetPlayer()->GetName(), reqtype);
                       }
                       // Hide and Seek Event
                       if (eventtype == 9)
                       {
                           char* eventname = "Hide and Seek Event";
                           QueryResult confirm = CharacterDatabase.PQuery("INSERT INTO `events` (`eventname`,`time`,`host`,`reqtype`) VALUES ('%s','%u','%s','%u')", eventname, timeSecstoMin, pPlayer->GetSession()->GetPlayer()->GetName(), reqtype);
                       }
                       // Hide and Seek Event
                       if (eventtype == 9)
                       {
                           char* eventname = "Hide and Seek Event";
                           QueryResult confirm = CharacterDatabase.PQuery("INSERT INTO `events` (`eventname`,`time`,`host`,`reqtype`) VALUES ('%s','%u','%s','%u')", eventname, timeSecstoMin, pPlayer->GetSession()->GetPlayer()->GetName(), reqtype);
                       }
                       // Trivia
                       if (eventtype == 10)
                       {
                           char* eventname = "Trivia Event";
                           QueryResult confirm = CharacterDatabase.PQuery("INSERT INTO `events` (`eventname`,`time`,`host`,`reqtype`) VALUES ('%s','%u','%s','%u')", eventname, timeSecstoMin, pPlayer->GetSession()->GetPlayer()->GetName(), reqtype);
                       }
                       // Fly or Die
                       if (eventtype == 11)
                       {
                           char* eventname = "Fly or Die Event";
                           QueryResult confirm = CharacterDatabase.PQuery("INSERT INTO `events` (`eventname`,`time`,`host`,`reqtype`) VALUES ('%s','%u','%s','%u')", eventname, timeSecstoMin, pPlayer->GetSession()->GetPlayer()->GetName(), reqtype);
                       }
                       // Scavenger Hunt
                       if (eventtype == 12)
                       {
                           char* eventname = "Scavenger Hunt Event";
                           QueryResult confirm = CharacterDatabase.PQuery("INSERT INTO `events` (`eventname`,`time`,`host`,`reqtype`) VALUES ('%s','%u','%s','%u')", eventname, timeSecstoMin, pPlayer->GetSession()->GetPlayer()->GetName(), reqtype);
                       }
                       // Kill GM
                       if (eventtype == 13)
                       {
                           char* eventname = "Kill the GM Event";
                           QueryResult confirm = CharacterDatabase.PQuery("INSERT INTO `events` (`eventname`,`time`,`host`,`reqtype`) VALUES ('%s','%u','%s','%u')", eventname, timeSecstoMin, pPlayer->GetSession()->GetPlayer()->GetName(), reqtype);
                       }
                       // Staircase Event
                       if (eventtype == 14)
                       {
                           char* eventname = "Staircase Event";
                           QueryResult confirm = CharacterDatabase.PQuery("INSERT INTO `events` (`eventname`,`time`,`host`,`reqtype`) VALUES ('%s','%u','%s','%u')", eventname, timeSecstoMin, pPlayer->GetSession()->GetPlayer()->GetName(), reqtype); 
                       }
                       // Transmog Event
                       if (eventtype == 15)
                       {
                           char* eventname = "Transmogrification Event";
                           QueryResult confirm = CharacterDatabase.PQuery("INSERT INTO `events` (`eventname`,`time`,`host`,`reqtype`) VALUES ('%s','%u','%s','%u')", eventname, timeSecstoMin, pPlayer->GetSession()->GetPlayer()->GetName(), reqtype);
                       }

                }
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Done!", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
                
            }

            // GM selected "Choose an event type"
            // Event Type Select Menu
            case EVENT_SELECT_TYPE:
            {
                
                // Check if GM Id exists for any new GM, if not create
                QueryResult findgmid = CharacterDatabase.PQuery("SELECT gmid FROM eventscache WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
                if (!findgmid)
                    {
                       QueryResult creategmid = CharacterDatabase.PQuery("INSERT INTO `eventscache` (`gmid`) VALUES ('%u')", pPlayer->GetSession()->GetGuidLow());    
                    }

                // PvP Events
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Choose: 1v1 Event", GOSSIP_SENDER_MAIN, EVENT_CHOOSE_1V1);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Choose: 2v2 Event", GOSSIP_SENDER_MAIN, EVENT_CHOOSE_2V2);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Choose: 3v3 Event", GOSSIP_SENDER_MAIN, EVENT_CHOOSE_3V3);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Choose: 4v4 Event", GOSSIP_SENDER_MAIN, EVENT_CHOOSE_4V4);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Choose: 5v5 Event", GOSSIP_SENDER_MAIN, EVENT_CHOOSE_5V5);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Choose: 10v10 Event", GOSSIP_SENDER_MAIN, EVENT_CHOOSE_10V10);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Choose: 20v20 Event", GOSSIP_SENDER_MAIN, EVENT_CHOOSE_20V20);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Choose: FFA Event", GOSSIP_SENDER_MAIN, EVENT_CHOOSE_FFA);
                // Non-PvP Events
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: Hide and Seek Event", GOSSIP_SENDER_MAIN, EVENT_CHOOSE_HNS);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: Trivia Event", GOSSIP_SENDER_MAIN, EVENT_CHOOSE_TRIV);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: Fly or Die Event", GOSSIP_SENDER_MAIN, EVENT_CHOOSE_FLYDIE);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: Kill the GM Event", GOSSIP_SENDER_MAIN, EVENT_CHOOSE_KILLGM);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: Scavenger Hunt Event", GOSSIP_SENDER_MAIN, EVENT_CHOOSE_SCAV);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: Staircase Event", GOSSIP_SENDER_MAIN, EVENT_CHOOSE_STAIRCASE);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: Transmogrification Event", GOSSIP_SENDER_MAIN, EVENT_CHOOSE_TRANSMOG);
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            }

            case EVENT_CHOOSE_1V1:
            {
               QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `eventtype`='1' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
               pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
               pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
               return true;         
               break;
            }
            case EVENT_CHOOSE_2V2:
            {
               QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `eventtype`='2' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
               pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
               pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
               return true;         
               break;
            }
            case EVENT_CHOOSE_3V3:
            {
               QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `eventtype`='3' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
               pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
               pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
               return true;         
               break;
            }
            case EVENT_CHOOSE_4V4:
            {
               QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `eventtype`='4' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
               pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
               pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
               return true;         
               break;
            }
            case EVENT_CHOOSE_5V5:
            {
               QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `eventtype`='5' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
               pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
               pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
               return true;         
               break;
            }
            case EVENT_CHOOSE_10V10:
            {
               QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `eventtype`='6' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
               pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
               pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
               return true;         
               break;
            }
            case EVENT_CHOOSE_20V20:
            {
               QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `eventtype`='7' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
               pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
               pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
               return true;         
               break;
            }
            case EVENT_CHOOSE_FFA:
            {
               QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `eventtype`='8' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
               pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
               pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
               return true;         
               break;
            }
            case EVENT_CHOOSE_HNS:
            {
               QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `eventtype`='9' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
               pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
               pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
               return true;         
               break;
            }
            case EVENT_CHOOSE_TRIV:
            {
               QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `eventtype`='10' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
               pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
               pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
               return true;         
               break;
            }
            case EVENT_CHOOSE_FLYDIE:
            {
               QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `eventtype`='11' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
               pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
               pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
               return true;         
               break;
            }
            case EVENT_CHOOSE_SCAV:
            {
               QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `eventtype`='12' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
               pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
               pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
               return true;         
               break;
            }
            case EVENT_CHOOSE_KILLGM:
            {
               QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `eventtype`='13' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
               pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
               pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
               return true;         
               break;
            }
            case EVENT_CHOOSE_STAIRCASE:
            {
               QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `eventtype`='14' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
               pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
               pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
               return true;         
               break;
            }
            case EVENT_CHOOSE_TRANSMOG:
            {
               QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `eventtype`='15' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
               pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
               pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
               return true;         
               break;
            }

            // Entry Requirements Menu
            case EVENT_SELECT_ENTRY:
            {
                // Check if GM Id exists for any new GM, if not create
                QueryResult findgmid = CharacterDatabase.PQuery("SELECT gmid FROM eventscache WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
                if (!findgmid)
                    {
                       QueryResult creategmid = CharacterDatabase.PQuery("INSERT INTO `eventscache` (`gmid`) VALUES ('%u')", pPlayer->GetSession()->GetGuidLow());    
                    }

                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: Dreadful Only", GOSSIP_SENDER_MAIN, EVENT_ENTRY_DREAD);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: Malevolent Only", GOSSIP_SENDER_MAIN, EVENT_ENTRY_MALEV);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: No PvE Trinkets Only", GOSSIP_SENDER_MAIN, EVENT_ENTRY_NOPVETR);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: No Entry Requirements", GOSSIP_SENDER_MAIN, EVENT_ENTRY_NONE);
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            }

            case EVENT_ENTRY_DREAD:
            {
                QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `reqtype`='1' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            }
            
            case EVENT_ENTRY_MALEV:
            {
                QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `reqtype`='2' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            }

            case EVENT_ENTRY_NOPVETR:
            {
                QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `reqtype`='3' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            }

            case EVENT_ENTRY_NONE:
            {
                QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `reqtype`='4' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            }

            // GM selected "Choose a event time"
            // Select time until event begins
            case EVENT_SELECT_TIMETIL:
            {
                // Check if GM Id exists for any new GM, if not create
                QueryResult findgmid = CharacterDatabase.PQuery("SELECT gmid FROM eventscache WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
                if (!findgmid)
                    {
                       QueryResult creategmid = CharacterDatabase.PQuery("INSERT INTO `eventscache` (`gmid`) VALUES ('%u')", pPlayer->GetSession()->GetGuidLow());    
                    }

                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: 20 minutes until event", GOSSIP_SENDER_MAIN, EVENT_TIME_20M);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: 30 minutes until event", GOSSIP_SENDER_MAIN, EVENT_TIME_30M);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: 40 minutes until event", GOSSIP_SENDER_MAIN, EVENT_TIME_40M);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: 50 minutes until event", GOSSIP_SENDER_MAIN, EVENT_TIME_50M);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: 1 hour until event", GOSSIP_SENDER_MAIN, EVENT_TIME_1H);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: 1 hour 30 minutes until event", GOSSIP_SENDER_MAIN, EVENT_TIME_1H30M);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Choose: 2 hours until event", GOSSIP_SENDER_MAIN, EVENT_TIME_2H);
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            }

            case EVENT_TIME_20M:
            {
                QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `timeleft`='20' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            }
            
            case EVENT_TIME_30M:
            {
                QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `timeleft`='30' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            }

            case EVENT_TIME_40M:
            {
                QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `timeleft`='40' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            }

            case EVENT_TIME_50M:
            {
                QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `timeleft`='50' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            }
            case EVENT_TIME_1H:
            {
                QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `timeleft`='60' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            }
            case EVENT_TIME_1H30M:
            {
                QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `timeleft`='90' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            }
            case EVENT_TIME_2H:
            {
                QueryResult choose = CharacterDatabase.PQuery("UPDATE eventscache SET `timeleft`='120' WHERE `gmid` = '%u'", pPlayer->GetSession()->GetGuidLow());
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "You have chosen it!", GOSSIP_SENDER_MAIN, EVENT_ORGANISE_MENU);
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            }

            ////
            // Event Management Menu
            ////

                
                // ==> Manage an Event Menu Structure
                // ===========================
                // - [Hide and Seek] scheduled in X minutes (Dreadful Only)
                // - Click here to re-broadcast your event (2 minute cooldown)
                // - Click here to delay your event (cannot be delayed if <5 min)
                // - Click here to change entry requirements
                // - Start your event (finishes timer) 
                //      - Finish your event (if started)

            case EVENT_MANAGE_MENU:
            {
                QueryResult getinfo = CharacterDatabase.PQuery("SELECT * FROM events WHERE `host` = '%s' && `started`='1' && `completed`='0'", pPlayer->GetSession()->GetPlayer()->GetName());
                if (getinfo)
                    {
                       Field * field = getinfo->Fetch();
                       std::string eventname = field[1].GetString();
                       std::string host = field[8].GetString();
                       int32 time = field[2].GetInt32();
                       int32 timetomin = time / 60;
                       uint32 reqtype = field[16].GetUInt32();
                       uint32 started = field[10].GetUInt32();
                       switch (reqtype){
                       
                       case 1: "Dreadful";
                       break;
                       case 2: "Malevolent";
                       break;
                       case 3: "No PvE Only";
                       break;
                       case 4: "No restrictions";
                       break; 
                       default: break;

                       }
                       std::stringstream buf;

                       buf << "[" << eventname << "] scheduled in |cff0000ff" << timetomin << "|r minutes (" << reqtype << ")";

                       pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, buf.str(), GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);
                       pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Click here to re-broadcast your event", GOSSIP_SENDER_MAIN, EVENT_MANAGE_REBROADCAST);
                      // pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Click here to delay your event", GOSSIP_SENDER_MAIN, EVENT_MANAGE_DELAY);
                     //  pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Click here to change entry requirements", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);
                       
                       if (started == 0)
                           pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Start your event", GOSSIP_SENDER_MAIN, EVENT_MANAGE_START);
                       else if (started == 1)
                           pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Finish your event", GOSSIP_SENDER_MAIN, EVENT_MANAGE_FINISH);
                    }

                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            }
            
            case EVENT_MANAGE_REBROADCAST:
            {

                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
               
            }

            case EVENT_MANAGE_START:
            {

                QueryResult getinfo = CharacterDatabase.PQuery("UPDATE events SET `started` = '1' WHERE `host` = '%s' && `completed`='0'", pPlayer->GetSession()->GetPlayer()->GetName());
                QueryResult selectid = CharacterDatabase.PQuery("SELECT `id`, `eventname`, `host` FROM events WHERE `host` = '%s' && `started`='1' && `completed`='0'", pPlayer->GetSession()->GetPlayer()->GetName());

                // Check if ID actually exists
                if (selectid)
                    {
                    Field * field = selectid->Fetch();
                    uint32 eventid = field[0].GetUInt32();
                    std::string eventname = field[1].GetString();
                    std::string host = field[2].GetString();
                   
                    pPlayer->GetSession()->SendNotification("|cff00c78c<GM Notice>|r |cffffcc00 An Event has now been started|r");
                    sWorld->SendWorldText(LANG_ANNOUNCE_STARTED, eventname.c_str(), host.c_str());
                    return true;
                    }
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            
            }

            case EVENT_MANAGE_FINISH:
            {

                QueryResult getinfo = CharacterDatabase.PQuery("UPDATE events SET `completed` = '1' WHERE `host` = '%s' && `started`='1'", pPlayer->GetSession()->GetPlayer()->GetName());
                pPlayer->GetSession()->SendNotification("|cff00c78c<GM Notice>|r |cffffcc00 An Event has now been finished|r");
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;
            
            }


            ////
>>>>>>> .merge_file_a13880
            // Teleportation Services
            ////

        case TELEPORTATION_SERVICES: 
            {

            if (pPlayer->GetTeam() == ALLIANCE)
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Alliance Mall", GOSSIP_SENDER_MAIN, TELEPORT_MALLS);
            if (pPlayer->GetTeam() == HORDE)
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Horde Mall", GOSSIP_SENDER_MAIN, TELEPORT_MALLS);
                
            if (pPlayer->GetSession()->GetSecurity() == 1)
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "V.I.P Mall", GOSSIP_SENDER_MAIN, TELEPORT_VIPMALL);

<<<<<<< .merge_file_a08136
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Icecrown FFA Arena |cffff0000(PvP)|r ", GOSSIP_SENDER_MAIN, TELEPORT_GURUBASHI);
=======
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Gurubashi Arena |cffff0000(PvP)|r ", GOSSIP_SENDER_MAIN, TELEPORT_GURUBASHI);
>>>>>>> .merge_file_a13880
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Dueling Zone", GOSSIP_SENDER_MAIN, TELEPORT_DUELZONE);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Goodbye...", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;         
                break;

        case TELEPORT_VIPMALL:
            if (pPlayer->GetSession()->GetSecurity() == 1)
                pPlayer->TeleportTo(870, 6596.368652f, 6101.055664f, 211.057999f, 3.612347f); // MapId, X, Y, Z, O
                break;

        case TELEPORT_GMISLAND:
            if (pPlayer->GetSession()->GetSecurity() > 1)
                pPlayer->TeleportTo(1, 16226.200195f, 16257.0000f, 13.202200f, 1.65007f); // MapId, X, Y, Z, O
                break;

        case TELEPORT_MALLS: // Malls
            if (pPlayer->GetTeam() == ALLIANCE)
                pPlayer->TeleportTo(870, 864.785095f, 284.378845f, 503.677734f, 0.638793f); // Alliance mall
            else
                pPlayer->TeleportTo(870, 1625.527466f, 924.170593f, 471.183441f, 3.250562f); // Horde Mall
            break;

<<<<<<< .merge_file_a08136
        case TELEPORT_GURUBASHI: // Icecrown Arena
            pPlayer->TeleportTo(571, 6747.201660f,1608.376221f,389.033020f,5.903505f);
            ChatHandler(pPlayer->GetSession()).PSendSysMessage("You have been teleported to the Icecrown Arena!");
=======
        case TELEPORT_GURUBASHI: // Gurubashi

            pPlayer->TeleportTo(0, -13251.451172f,185.116165f,31.7887171f,1.106553f);
            ChatHandler(pPlayer->GetSession()).PSendSysMessage("You have been teleported to the Gurubashi Arena!");
>>>>>>> .merge_file_a13880
            break;

        case TELEPORT_DUELZONE: // Duel Zone
            pPlayer->TeleportTo(870,3984.989014f,1879.952515f,904.331909f,4.086052f);
            ChatHandler(pPlayer->GetSession()).PSendSysMessage("You have been teleported to the Duel Zone!");
            break;

            //////
            // Transmogrification Services
            //////
        case TRANSMOG_SERVICES: 
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Free Transmogrification Zone", GOSSIP_SENDER_MAIN, TELEPORT_TRANSMOG);
                // Check Reputation before showing this
                if (pPlayer->GetReputationRank(1302) != REP_EXALTED)
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Max Mists of Pandaria Reputation", GOSSIP_SENDER_MAIN, TRANSMOG_REPUTATION);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Goodbye...", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);
                pPlayer->PlayerTalkClass->SendGossipMenu(TRANSMOG_TEXTS, pCreature->GetGUID());
                return true;
                break;

        case TELEPORT_TRANSMOG: // Transmogrification Zone
            pPlayer->TeleportTo(571,7432.168457f,-533.460693f,1896.850830f,1.365842f);
            ChatHandler(pPlayer->GetSession()).PSendSysMessage("You have been teleported to the Free Transmogrifcation Zone!");
            break;

        case TRANSMOG_REPUTATION: // Reputation Max
            pPlayer->SetReputation(1302,REPUTATION_REQUIRED); // The Tillers
            pPlayer->SetReputation(1272,REPUTATION_REQUIRED); // The Anglers
            pPlayer->SetReputation(1270,REPUTATION_REQUIRED); // Shado Pan
            pPlayer->SetReputation(1271,REPUTATION_REQUIRED); // Order of the Cloud Serpent
            pPlayer->SetReputation(1345,REPUTATION_REQUIRED); // The Lorewalkers
            pPlayer->SetReputation(1337,REPUTATION_REQUIRED); // The Klaxxi
            pPlayer->SetReputation(1269,REPUTATION_REQUIRED); // Golden Lotus
            pPlayer->SetReputation(1341,REPUTATION_REQUIRED); // August Celestials
            ChatHandler(pPlayer->GetSession()).PSendSysMessage("Reputation for Mists of Pandaria has been maxed out.");
            pPlayer->PlayerTalkClass->SendCloseGossip();
            break;

            //////
            // Pet Tame Teleport (In Main Menu on its own)
            //////
        case TELEPORT_PETZONE: // Pet Mall
            pPlayer->TeleportTo(571,8432.795898f,895.314758f,544.674805f,1.592192f);
            ChatHandler(pPlayer->GetSession()).PSendSysMessage("You have been teleported to the Pet Mall!");
            break;

            //////
            // Event Services
            /////
            //////

            // Shows all scheduled events
        case SCHEDULED_EVENTS_LIST:

            {
<<<<<<< .merge_file_a08136
                QueryResult scheduledlist = CharacterDatabase.PQuery("SELECT `id`, `eventname`, `host` FROM events WHERE `started` = '0' && `completed` = '0'");
=======
                QueryResult scheduledlist = CharacterDatabase.PQuery("SELECT `id`, `eventname`, `host`, `time` FROM events WHERE `started` = '0' && `completed` = '0'");
>>>>>>> .merge_file_a13880
                if (!scheduledlist)
                {
                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "No Upcoming Scheduled Events", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);
                    
                }
                else
                {
                    do
                    {
                        Field * field = scheduledlist->Fetch();
                        uint32 eventid = field[0].GetUInt32();
                        std::string eventname = field[1].GetString();
                        std::string host = field[2].GetString();
                        std::stringstream buf;
                        std::stringstream buf2;
<<<<<<< .merge_file_a08136

                        buf << eventname << " - |cff0000ff<GM> " << host << "|r";
=======
                        uint32 time = field[3].GetUInt32();
                        int32 timetomin = time / 60;

                        buf << eventname << " - |cff0000ff<GM> " << host << "|r in " << timetomin << " minutes.";
>>>>>>> .merge_file_a13880
                        buf2 << "Sign Up feature coming soon!";

                        pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, buf.str(), GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE, buf2.str(), 0, false);
                    }
                    while(scheduledlist->NextRow());
                    
                }
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Goodbye...", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE); 
            }

            pPlayer->PlayerTalkClass->SendGossipMenu(EVENT_TEXTS, pCreature->GetGUID());
            return true;
            break;
            // Shows all active events
        case ACTIVE_EVENTS_LIST:
            
            {
                QueryResult activescheduledlist = CharacterDatabase.PQuery("SELECT `id`, `eventname`, `host` FROM events WHERE `started` = '1' && `completed` = '0'");
                if (!activescheduledlist)
                {
                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "No Upcoming Scheduled Events", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);
                }
                else
                {
                    do
                    {
                        Field * field = activescheduledlist->Fetch();
                        uint32 eventid = field[0].GetUInt32();
                        std::string eventname = field[1].GetString();
                        std::string host = field[2].GetString();
                        std::stringstream buf;
                        std::stringstream buf2;

                        buf << eventname << " - |cff0000ff<GM> " << host << "|r";
                        buf2 << "Spectate Feature coming soon!";

                        pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, buf.str(), GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE, buf2.str(), 0, false);
                    }
                    while(activescheduledlist->NextRow());
                    
                    
                }
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Goodbye...", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);
                
            }
            pPlayer->PlayerTalkClass->SendGossipMenu(EVENT_TEXTS, pCreature->GetGUID());
            return true;
            break;

            //////
            // FAQ
            //////

            case INFORMATION_FAQ_SERVICE:
            {
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "General Inquiries", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_GENERAL);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Bugs and Glitches", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_BUGS);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "VIP and Donation Inquiries", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_DONATION);
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK,"Other Inquiries", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_SERVICE, "Simply make a ticket and a GM will assist you as soon as possible.",0, false);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Goodbye...", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);

                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;
                break;
            }

            case INFORMATION_FAQ_GENERAL:
            {
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK,"[Q] What sort of server is this?", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_GENERAL, "Arena Source is an Instant 90 Battleground/Arena PvP Server.",0, false);
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK,"[Q] Where do I get gear?", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_GENERAL, "Gear can be bought from the malls located in Teleporter Services. Honor Points/Conquest Points are used to upgrade gear.",0, false);
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK,"[Q] How do I get Honor Points/Conquest Points", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_GENERAL, "Honor Points are obtained through Battlegrounds/1v1's only. Conquest Points are obtained in 2v2/3v3/5v5 only.",0, false);
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK,"[Q] How do I get Transmogrification?", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_GENERAL, "Transmogrification Gear can be obtained by using the online web transmog store, or by visiting the Transmogrification Tab in the Services.",0, false);
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK,"[Q] I have a good suggestion, how do I let you know?", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_GENERAL, "Visit www.arena-source.com/forum and login with your in-game credentials; you can post your suggestions on the forum suggestions section!",0, false);
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK,"[Q] Is Multiboxing allowed?", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_GENERAL, "Currently, there are no rules about Multiboxing, however this may subject to change in the future.",0, false);
                    
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "<-- Go Back", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_SERVICE);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Goodbye...", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);
                
                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;
                break;
            }

            case INFORMATION_FAQ_BUGS:
            {
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK,"[Q] I just found a bug, where do I report it?", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_BUGS, "You can report all bugs at www.arena-source.com/issues - simply register a new account (takes 10 seconds) and report the bug. A developer will inspect it within at least 24 hours to acknowledge it.",0, false);
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK,"[Q] Can I get banned for abusing a bug?", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_BUGS, "Abusing bugs, especially to gain an unfair advantage in-game towards other players will not be tolerated. If you find a bug, then simply report it and it will be fixed, so that other players don't abuse it.",0, false);
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK,"[Q] Is a 'glitch' counted as a bug?", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_BUGS, "Glitches are simply bugs which require an extensive method to reproduce, therefore should be reported on the bugtracker.",0, false);
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK,"[Q] Do all bugs get fixed?", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_BUGS, "All bugs will get fixed in time, no bug has ever been left behind, we ask to be patient whilst we work on every bug.",0, false);
                
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "<-- Go Back", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_SERVICE);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Goodbye...", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);

                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;
                break;
            }
            
            case INFORMATION_FAQ_DONATION:
            {
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK,"[Q] How do I donate?", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_DONATION, "You can donate simply by going to www.arena-source.com and then click on 'Donate' at the side of the page. Currently we accept PayPal only.",0, false);
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK,"[Q] What is the purpose of donation?", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_DONATION, "Donations from the community help keep the server active and running everyday; so by helping the server, players are rewarded with Donation Points in return.",0, false);
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK,"[Q] Will there be an option to donate via. SMS soon?", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_DONATION, "Unfortunately, we will not have SMS available as it causes too many complications between the user and us, in terms of payment.",0, false);
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK,"[Q] What is V.I.P?", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_DONATION, "V.I.P grants access to full malevolent on all characters on a single account for a lifetime, as well as other perks. This can be bought with donation points.",0, false);
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK,"[Q] Does VIP grant a PvP advantage towards others?", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_DONATION, "No. We have specifically designed V.I.P to give no unobtainable gear/buffs whatsoever that would lead to unbalanced PvP.",0, false);
               
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "<-- Go Back", GOSSIP_SENDER_MAIN, INFORMATION_FAQ_SERVICE);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Goodbye...", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);

                pPlayer->PlayerTalkClass->SendGossipMenu(TELEPORTATION_TEXTS, pCreature->GetGUID());
                return true;
                break;
            }

            // Goodbye
            //////
        case 5000:
            pPlayer->PlayerTalkClass->SendCloseGossip();
            break;
            }
            return true;
        }
        return true;
    }
};

void AddSC_global_teleporter()
{
new global_teleporter();
}