///////////////////////
// Rival System: Features Solo Queue
//// Developed by Cakes
//// For: Arena-Source.com
///////////////////////

#include "ScriptMgr.h"
#include "ArenaTeamMgr.h"
#include "Common.h"
#include "DisableMgr.h"
#include "BattlegroundMgr.h"
#include "Battleground.h"
#include "ArenaTeam.h"
#include "Language.h"
#include "npc_arena1v1.h"

enum enus
{
	GOSSIP_GOODBYE = 1000,

	// NPC Texts
	GOSSIP_FIRST_MENU_TEXT   = 13371337,
	GOSSIP_1v1_MENU_TEXT     = 13371336,
	GOSSIP_SOLO_Q_MENU_TEXT  = 13371335,

	// 1v1 //
	GOSSIP_1v1_MENU          = 1337,
	GOSSIP_1v1_CREATE_TEAM   = 1,
	GOSSIP_1v1_JOIN_RANKED   = 2,
	GOSSIP_1v1_JOIN_UNRATED  = 3,
	GOSSIP_1v1_STATS         = 4,

    // Solo Queueing //

	GOSSIP_SOLO_Q_MENU       = 1338,
	     
	// 2v2 Solo Queues //
    GOSSIP_2v2s_CREATE_TEAM  = 5,
	GOSSIP_2v2s_JOIN_RANKED  = 6,
	GOSSIP_2v2s_JOIN_UNRATED = 7,

	// 3v3 Solo Queues //
	GOSSIP_3v3s_CREATE_TEAM  = 8,
	GOSSIP_3v3s_JOIN_RANKED  = 9,
	GOSSIP_3v3s_JOIN_UNRATED = 10,

	// 5v5 Solo Queues //
	GOSSIP_5v5s_CREATE_TEAM  = 11,
	GOSSIP_5v5s_JOIN_RANKED  = 12,
	GOSSIP_5v5s_JOIN_UNRATED = 13,

};

class npc_1v1arena : public CreatureScript
{
public:
    npc_1v1arena() : CreatureScript("npc_1v1arena") 
	{
	}

	// 1v1 Functions

	bool JoinQueueArena1v1(Player* player, Creature* me, bool isRated)
	{

		if(!player || !me)
			return false;

		// Check if character is the minimum level for Arenas
		if(sWorld->getIntConfig(CONFIG_ARENA_1V1_MIN_LEVEL) > player->getLevel())
			return false;

		// Get GUID
		uint64 guid = player->GetGUID();
		// Get the Arena Team Slot
		uint8 arenaslot = ArenaTeam::GetSlotByType(ARENA_TEAM_1v1);
		// Get the type of Arena
		uint8 arenatype = ARENA_TYPE_1v1;
		// Set default arena ratings/mmr
		uint32 arenaRating = 0;
		uint32 matchmakerRating = 0;

		// Check if the player is already queued for a battleground
		if (player->InBattleground())
			return false;

		// Check if All Arena's is in battleground_template
		Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(BATTLEGROUND_AA);
		if (!bg)
		{
			sLog->OutPandashan("All Arena's in battleground_template doesn't exist!");
			return false;
		}

		// Check if All Arena's is disabled
		if (DisableMgr::IsDisabledFor(DISABLE_TYPE_BATTLEGROUND, BATTLEGROUND_AA, NULL))
		{
			ChatHandler(player->GetSession()).PSendSysMessage(LANG_ARENA_DISABLED);
			return false;
		}

		// Get battleground type and arena type
		BattlegroundTypeId bgTypeId = bg->GetTypeID();
		BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(bgTypeId, arenatype);
		PvPDifficultyEntry const* bracketEntry = GetBattlegroundBracketByLevel(bg->GetMapId(), player->getLevel());
		if (!bracketEntry)
			return false;

		GroupJoinBattlegroundResult err = ERR_BATTLEGROUND_NONE;

		// Check if the player is in a queue
        if (player->GetBattlegroundQueueIndex(bgQueueTypeId) < PLAYER_MAX_BATTLEGROUND_QUEUES)
            // Player is already in this queue
            return false;
        // Check if they have free slots
        if (!player->HasFreeBattlegroundQueueId())
            return false;

		uint32 ateamId = 0;

		if(isRated)
		{
			// Get arena team ID if it's rated
			
			ateamId = player->GetArenaTeamId(arenaslot);
			ArenaTeam* at = sArenaTeamMgr->GetArenaTeamById(ateamId);
			if (!at)
			{
				player->GetSession()->SendNotInArenaTeamPacket(arenatype); // Player didn't have the correct arena team for this queue
				return false;
			}

			// Get 1v1 team rating
			arenaRating = at->GetRating();
			matchmakerRating = arenaRating;

			 // Force queues to trigger for ranked
			 if (arenaRating < 100)
				arenaRating = 100;	 
		}

		BattlegroundQueue &bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId);
		bg->SetRated(isRated);
		
        GroupQueueInfo* ginfo;
		ginfo = bgQueue.AddGroup(player, NULL, bgTypeId, bracketEntry, arenatype, isRated, false, arenaRating, matchmakerRating, ateamId);
        uint32 avgTime = bgQueue.GetAverageQueueWaitTime(ginfo, bracketEntry->GetBracketId());
        // Add to the queue handler
		uint32 queueSlot = player->AddBattlegroundQueueId(bgQueueTypeId);
        // Add a join time
		player->AddBattlegroundQueueJoinTime(bgTypeId, ginfo->JoinTime);
        WorldPacket data;
        // Send to queue handler that we're in queue
		
        sBattlegroundMgr->BuildBattlegroundStatusPacket(&data, bg, player, queueSlot, STATUS_WAIT_QUEUE, avgTime, ginfo->JoinTime, arenatype, 0);
        player->GetSession()->SendPacket(&data);
		// Send data to build our battleground
		// Send that we're waiting for a queue
        
		sLog->OutPandashan("Battleground: Player has joined queue for 1v1. BG Queue Type: %u . BG type: %u. Player GUID: %u.");

		sBattlegroundMgr->ScheduleQueueUpdate(matchmakerRating, arenatype, bgQueueTypeId, bgTypeId, bracketEntry->GetBracketId());

		return true;
		
	}

	bool CreateArenateam(Player* player, Creature* me)
	{
		if(!player || !me)
			return false;

		uint8 slot = ArenaTeam::GetSlotByType(ARENA_TEAM_1v1);
		if (slot >= MAX_ARENA_SLOT)
			return false;

		// Check if player is already in an arena team
		if (player->GetArenaTeamId(slot))
		{
			player->GetSession()->SendArenaTeamCommandResult(ERR_ARENA_TEAM_CREATE_S, player->GetName(), "", ERR_ALREADY_IN_ARENA_TEAM);
			return false;
		}

		// Teamname = playername
		// if teamname exist, we have to choose another name (playername + number)
		int i = 1;
		std::stringstream teamName;
		teamName << player->GetName();

		// Create arena team
		ArenaTeam* arenaTeam = new ArenaTeam();

		if (!arenaTeam->Create(player->GetGUID(), ARENA_TEAM_1v1, teamName.str(), 4283124816, 45, 4294242303, 5, 4294705149))
		{
			delete arenaTeam;
			return false;
		}

		// Register arena team
		sArenaTeamMgr->AddArenaTeam(arenaTeam);
		arenaTeam->AddMember(player->GetGUID());

		ChatHandler(player->GetSession()).SendSysMessage("You've now signed up to the Rival League!");

		return true;
	}

	// 2v2 Functions

	bool JoinQueueArena2v2s(Player* player, Creature* me, bool isRated)
	{

		// Get GUID
		uint64 guid = player->GetGUID();
		// Get the Arena Team Slot
		uint8 arenaslot = ArenaTeam::GetSlotByType(ARENA_TEAM_2v2s);
		// Get the type of Arena
		uint8 arenatype = ARENA_TYPE_2v2s;
		// Set default arena ratings/mmr
		uint32 arenaRating = 0;
		uint32 matchmakerRating = 0;

		// Check if the player is already queued for a battleground
		if (player->InBattleground())
			return false;

		// Check if All Arena's is in battleground_template
		Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(BATTLEGROUND_AA);
		if (!bg)
		{
			return false;
		}

		// Check if All Arena's is disabled
		if (DisableMgr::IsDisabledFor(DISABLE_TYPE_BATTLEGROUND, BATTLEGROUND_AA, NULL))
		{
			ChatHandler(player->GetSession()).PSendSysMessage(LANG_ARENA_DISABLED);
			return false;
		}

		// Get battleground type and arena type
		BattlegroundTypeId bgTypeId = bg->GetTypeID();

		BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(bgTypeId, arenatype);

		PvPDifficultyEntry const* bracketEntry = GetBattlegroundBracketByLevel(bg->GetMapId(), player->getLevel());

		GroupJoinBattlegroundResult err = ERR_BATTLEGROUND_NONE;

		uint32 ateamId = 0;

		if(isRated)
		{
			// Get arena team ID if it's rated
			
			ateamId = player->GetArenaTeamId(arenaslot);

			ArenaTeam* at = sArenaTeamMgr->GetArenaTeamById(ateamId);

			if (!at)
			{
				player->GetSession()->SendNotInArenaTeamPacket(arenatype); // Player didn't have the correct arena team for this queue - Shouldn't happen
				return false;
			}

			// Get 2v2s team rating
			arenaRating = at->GetRating();
			matchmakerRating = arenaRating;
			sLog->OutPandashan("Obtained matchmakerRating: %u", matchmakerRating);
			 // Force queues to trigger for ranked
			 if (arenaRating < 100)
				arenaRating = 100;	 
		}

		BattlegroundQueue &bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId);
		bg->SetRated(isRated);
		
        GroupQueueInfo* ginfo;
		ginfo = bgQueue.AddGroup(player, NULL, bgTypeId, bracketEntry, arenatype, isRated, false, arenaRating, matchmakerRating, ateamId);
        uint32 avgTime = bgQueue.GetAverageQueueWaitTime(ginfo, bracketEntry->GetBracketId());

        // Add to the queue handler
		uint32 queueSlot = player->AddBattlegroundQueueId(bgQueueTypeId);
        // Add a join time
		player->AddBattlegroundQueueJoinTime(bgTypeId, ginfo->JoinTime);

        WorldPacket data;

        // Send to queue handler that we're in queue
		
        sBattlegroundMgr->BuildBattlegroundStatusPacket(&data, bg, player, queueSlot, STATUS_WAIT_QUEUE, avgTime, ginfo->JoinTime, arenatype, 0);

        player->GetSession()->SendPacket(&data);

		// Send data to build our battleground
		// Send that we're waiting for a queue
		sBattlegroundMgr->ScheduleQueueUpdate(matchmakerRating, arenatype, bgQueueTypeId, bgTypeId, bracketEntry->GetBracketId());

		return true;
		
	}

	bool CreateArenateam2v2s(Player* player, Creature* me)
	{
		if(!player || !me)
			return false;

		uint8 slot = ArenaTeam::GetSlotByType(ARENA_TEAM_2v2s);
		if (slot >= MAX_ARENA_SLOT)
			return false;

		
		// Check if player is already in an arena team
		if (player->GetArenaTeamId(slot))
		{
			
			player->GetSession()->SendArenaTeamCommandResult(ERR_ARENA_TEAM_CREATE_S, player->GetName(), "", ERR_ALREADY_IN_ARENA_TEAM);
			return false;
		}

		int i = 1;
		std::stringstream teamName;
		teamName << player->GetName();

		// Create arena team
		ArenaTeam* arenaTeam = new ArenaTeam();

		if (!arenaTeam->Create(player->GetGUID(), ARENA_TEAM_2v2s, teamName.str(), 4283124816, 45, 4294242303, 5, 4294705149))
		{
			delete arenaTeam;
			return false;
		}

		// Register arena team
		sArenaTeamMgr->AddArenaTeam(arenaTeam);
		arenaTeam->AddMember(player->GetGUID());

		ChatHandler(player->GetSession()).SendSysMessage("You've signed up to 2v2 solo queue!");

		return true;
	}

	// 3v3 Functions

	bool JoinQueueArena3v3s(Player* player, Creature* me, bool isRated)
	{

		// Get GUID
		uint64 guid = player->GetGUID();
		// Get the Arena Team Slot
		uint8 arenaslot = ArenaTeam::GetSlotByType(ARENA_TEAM_3v3s);
		// Get the type of Arena
		uint8 arenatype = ARENA_TYPE_3v3s;
		// Set default arena ratings/mmr
		uint32 arenaRating = 0;
		uint32 matchmakerRating = 0;

		// Check if the player is already queued for a battleground
		if (player->InBattleground())
			return false;

		// Check if All Arena's is in battleground_template
		Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(BATTLEGROUND_AA);
		if (!bg)
		{
			return false;
		}

		// Check if All Arena's is disabled
		if (DisableMgr::IsDisabledFor(DISABLE_TYPE_BATTLEGROUND, BATTLEGROUND_AA, NULL))
		{
			ChatHandler(player->GetSession()).PSendSysMessage(LANG_ARENA_DISABLED);
			return false;
		}

		// Get battleground type and arena type
		BattlegroundTypeId bgTypeId = bg->GetTypeID();
     
		BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(bgTypeId, arenatype);

		PvPDifficultyEntry const* bracketEntry = GetBattlegroundBracketByLevel(bg->GetMapId(), player->getLevel());

		GroupJoinBattlegroundResult err = ERR_BATTLEGROUND_NONE;

		uint32 ateamId = 0;
		if(isRated)
		{
			// Get arena team ID if it's rated
			
			ateamId = player->GetArenaTeamId(arenaslot);
			ArenaTeam* at = sArenaTeamMgr->GetArenaTeamById(ateamId);
			if (!at)
			{
				player->GetSession()->SendNotInArenaTeamPacket(arenatype); // Player didn't have the correct arena team for this queue
				return false;
			}

			// Get 2v2s team rating
			arenaRating = at->GetRating();
			matchmakerRating = arenaRating;
			 // Force queues to trigger for ranked
			 if (arenaRating < 100)
				arenaRating = 100;	 
		}

		BattlegroundQueue &bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId);
		bg->SetRated(isRated);
		
        GroupQueueInfo* ginfo;
		ginfo = bgQueue.AddGroup(player, NULL, bgTypeId, bracketEntry, arenatype, isRated, false, arenaRating, matchmakerRating, ateamId);
        uint32 avgTime = bgQueue.GetAverageQueueWaitTime(ginfo, bracketEntry->GetBracketId());

        // Add to the queue handler
		uint32 queueSlot = player->AddBattlegroundQueueId(bgQueueTypeId);
        // Add a join time
		player->AddBattlegroundQueueJoinTime(bgTypeId, ginfo->JoinTime);

        WorldPacket data;
        // Send to queue handler that we're in queue
		
        sBattlegroundMgr->BuildBattlegroundStatusPacket(&data, bg, player, queueSlot, STATUS_WAIT_QUEUE, avgTime, ginfo->JoinTime, arenatype, 0);
        player->GetSession()->SendPacket(&data);

		// Send data to build our battleground
		// Send that we're waiting for a queue
		sBattlegroundMgr->ScheduleQueueUpdate(matchmakerRating, arenatype, bgQueueTypeId, bgTypeId, bracketEntry->GetBracketId());

		return true;
	}

	bool CreateArenateam3v3s(Player* player, Creature* me)
	{
		if(!player || !me)
			return false;

		uint8 slot = ArenaTeam::GetSlotByType(ARENA_TEAM_3v3s);
		if (slot >= MAX_ARENA_SLOT)
			return false;

		// Check if player is already in an arena team
		if (player->GetArenaTeamId(slot))
		{
			player->GetSession()->SendArenaTeamCommandResult(ERR_ARENA_TEAM_CREATE_S, player->GetName(), "", ERR_ALREADY_IN_ARENA_TEAM);
			return false;
		}

		int i = 1;
		std::stringstream teamName;
		teamName << player->GetName();

		// Create arena team
		ArenaTeam* arenaTeam = new ArenaTeam();

		if (!arenaTeam->Create(player->GetGUID(), ARENA_TEAM_3v3s, teamName.str(), 4283124816, 45, 4294242303, 5, 4294705149))
		{
			delete arenaTeam;
			return false;
		}

		// Register arena team
		sArenaTeamMgr->AddArenaTeam(arenaTeam);
		arenaTeam->AddMember(player->GetGUID());

		ChatHandler(player->GetSession()).SendSysMessage("You've signed up to 3v3 solo queue!");

		return true;
	}

	// 5v5 Functions

	bool JoinQueueArena5v5s(Player* player, Creature* me, bool isRated)
	{

		// Get GUID
		uint64 guid = player->GetGUID();
		// Get the Arena Team Slot
		uint8 arenaslot = ArenaTeam::GetSlotByType(ARENA_TEAM_5v5s);
		// Get the type of Arena
		uint8 arenatype = ARENA_TYPE_5v5s;
		// Set default arena ratings/mmr
		uint32 arenaRating = 0;
		uint32 matchmakerRating = 0;

		// Check if the player is already queued for a battleground
		if (player->InBattleground())
			return false;

		// Check if All Arena's is in battleground_template
		Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(BATTLEGROUND_AA);
		if (!bg)
		{
			return false;
		}

		// Check if All Arena's is disabled
		if (DisableMgr::IsDisabledFor(DISABLE_TYPE_BATTLEGROUND, BATTLEGROUND_AA, NULL))
		{
			ChatHandler(player->GetSession()).PSendSysMessage(LANG_ARENA_DISABLED);
			return false;
		}

		// Get battleground type and arena type
		BattlegroundTypeId bgTypeId = bg->GetTypeID();
     
		BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(bgTypeId, arenatype);

		PvPDifficultyEntry const* bracketEntry = GetBattlegroundBracketByLevel(bg->GetMapId(), player->getLevel());

		GroupJoinBattlegroundResult err = ERR_BATTLEGROUND_NONE;

		uint32 ateamId = 0;
		if(isRated)
		{
			// Get arena team ID if it's rated
			
			ateamId = player->GetArenaTeamId(arenaslot);
			ArenaTeam* at = sArenaTeamMgr->GetArenaTeamById(ateamId);
			if (!at)
			{
				player->GetSession()->SendNotInArenaTeamPacket(arenatype); // Player didn't have the correct arena team for this queue
				return false;
			}

			// Get 2v2s team rating
			arenaRating = at->GetRating();
			matchmakerRating = arenaRating;
			 // Force queues to trigger for ranked
			 if (arenaRating < 100)
				arenaRating = 100;	 
		}

		BattlegroundQueue &bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId);
		bg->SetRated(isRated);
		
        GroupQueueInfo* ginfo;
		ginfo = bgQueue.AddGroup(player, NULL, bgTypeId, bracketEntry, arenatype, isRated, false, arenaRating, matchmakerRating, ateamId);
        uint32 avgTime = bgQueue.GetAverageQueueWaitTime(ginfo, bracketEntry->GetBracketId());

        // Add to the queue handler
		uint32 queueSlot = player->AddBattlegroundQueueId(bgQueueTypeId);
        // Add a join time
		player->AddBattlegroundQueueJoinTime(bgTypeId, ginfo->JoinTime);

        WorldPacket data;
        // Send to queue handler that we're in queue
		
        sBattlegroundMgr->BuildBattlegroundStatusPacket(&data, bg, player, queueSlot, STATUS_WAIT_QUEUE, avgTime, ginfo->JoinTime, arenatype, 0);
        player->GetSession()->SendPacket(&data);

		// Send data to build our battleground
		// Send that we're waiting for a queue
		sBattlegroundMgr->ScheduleQueueUpdate(matchmakerRating, arenatype, bgQueueTypeId, bgTypeId, bracketEntry->GetBracketId());

		return true;
	}

	// Menus etc

	bool OnGossipHello(Player* player, Creature* me)
	{
		if(!player || !me)
			return true;

		if(sWorld->getBoolConfig(CONFIG_ARENA_1V1_ENABLE) == false)
		{
			ChatHandler(player->GetSession()).SendSysMessage("Arena Battlemaster Disabled!");
			return true;
		}
        
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Participate in 1v1 Arena Brawling", GOSSIP_SENDER_MAIN, GOSSIP_1v1_MENU);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Participate in Solo Queue Arenas", GOSSIP_SENDER_MAIN, GOSSIP_SOLO_Q_MENU);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Nevermind", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);
		player->SEND_GOSSIP_MENU(GOSSIP_FIRST_MENU_TEXT, me->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* me, uint32 /*uiSender*/, uint32 uiAction)
	{
		if(!player || !me)
			return true;

		player->PlayerTalkClass->ClearMenus();

		switch (uiAction)
        {
			////////////////////////
			/////// 1v1 System 
			////////////////////////

		case GOSSIP_GOODBYE: // Goodbye
			{
				player->CLOSE_GOSSIP_MENU();
				break;
			}

		case GOSSIP_1v1_MENU:
		 {
				// Currently in Queue for Brawl
	    	if(player->InBattlegroundQueueForBattlegroundQueueType(BATTLEGROUND_QUEUE_1v1))
		    		player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_BATTLE, "You are currently in a queue for a Brawl", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE, "You can still back out by right clicking on the queue icon on the minimap", 0, false);
	    	else
                // Show if not in a queue already for a Brawl
		    	player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Queue up for a practice Brawl (Unrated)", GOSSIP_SENDER_MAIN, GOSSIP_1v1_JOIN_UNRATED);

	    	if(player->GetArenaTeamId(ArenaTeam::GetSlotByType(ARENA_TEAM_1v1)) == NULL)
		    	player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "Sign me up to the Rival League!", GOSSIP_SENDER_MAIN, GOSSIP_1v1_CREATE_TEAM, "When you're signed up, you can participate in Rated Brawls!", sWorld->getIntConfig(CONFIG_ARENA_1V1_COSTS), false);
	    	else
	    	{
		     	if(player->InBattlegroundQueueForBattlegroundQueueType(BATTLEGROUND_QUEUE_1v1) == false)
		    	{
			    	player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_BATTLE, "Queue up for a Brawl (Ranked)", GOSSIP_SENDER_MAIN, GOSSIP_1v1_JOIN_RANKED, "Warning: Joining Ranked Brawls means you cannot reset your rating! Are you ready?", 0, false);
		    	}

		    	player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Show your Rival League Statistics", GOSSIP_SENDER_MAIN, 4);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Nevermind", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);

		    }
			
			player->SEND_GOSSIP_MENU(GOSSIP_1v1_MENU_TEXT, me->GetGUID());
			break;

		 }
		 
		 	////////////////////////
			/////// Solo Queues
			////////////////////////

		case GOSSIP_SOLO_Q_MENU:
		 {
				// Currently in Queue for 2v2 solo queue
	    	if(player->InBattlegroundQueueForBattlegroundQueueType(BATTLEGROUND_QUEUE_2v2s))
		    		player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_BATTLE, "You are currently in a queue for a 2v2 Solo Queue", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE, "You can still back out by right clicking on the queue icon on the minimap", 0, false);
	    	else
			{
                // Show if not in a queue already for a queue
		    	player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Join: Unrated 2v2 Solo Queue", GOSSIP_SENDER_MAIN, GOSSIP_2v2s_JOIN_UNRATED);
			}

			if(player->InBattlegroundQueueForBattlegroundQueueType(BATTLEGROUND_QUEUE_3v3s))
				player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_BATTLE, "You are currently in a queue for a 3v3 Solo Queue", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE, "You can still back out by right clicking on the queue icon on the minimap", 0, false);
			else
			{
			    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Join: Unrated 3v3 Solo Queue", GOSSIP_SENDER_MAIN, GOSSIP_3v3s_JOIN_UNRATED);
			}

            if(player->InBattlegroundQueueForBattlegroundQueueType(BATTLEGROUND_QUEUE_5v5s))
				player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_BATTLE, "You are currently in a queue for a 5v5 Solo Queue", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE, "You can still back out by right clicking on the queue icon on the minimap", 0, false);
			else
			{
			    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Join: Unrated 5v5 Solo Queue", GOSSIP_SENDER_MAIN, GOSSIP_5v5s_JOIN_UNRATED);
			}

			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Nevermind", GOSSIP_SENDER_MAIN, GOSSIP_GOODBYE);
			player->SEND_GOSSIP_MENU(GOSSIP_SOLO_Q_MENU_TEXT, me->GetGUID());
			break;

		 }

		// 1v1 gossip functions

		case GOSSIP_1v1_CREATE_TEAM: // Create new 1v1 arena team
			{
				if(sWorld->getIntConfig(CONFIG_ARENA_1V1_MIN_LEVEL) <= player->getLevel())
				{
					if(player->GetMoney() >= sWorld->getIntConfig(CONFIG_ARENA_1V1_COSTS) && CreateArenateam(player, me))
						player->ModifyMoney(sWorld->getIntConfig(CONFIG_ARENA_1V1_COSTS) * -1);
				}
				else
				{
					ChatHandler(player->GetSession()).PSendSysMessage("You need level %u+ to create an 1v1 arenateam.", sWorld->getIntConfig(CONFIG_ARENA_1V1_MIN_LEVEL));
					player->CLOSE_GOSSIP_MENU();
					return true;
				}
				break;
			}

		case GOSSIP_1v1_JOIN_RANKED: // Join 1v1 Queue Arena (rated)
			{
				{
				if(Arena1v1CheckTalents(player) && JoinQueueArena1v1(player, me, true) == false)
					ChatHandler(player->GetSession()).SendSysMessage("Something went wrong while join queue.");
				
				player->CLOSE_GOSSIP_MENU();
				return true;
				}
				break;

			}

		case GOSSIP_1v1_JOIN_UNRATED: // Join 1v1 Queue Arena (unrated)
			{
				{
				if(Arena1v1CheckTalents(player) && JoinQueueArena1v1(player, me, false) == false)
					ChatHandler(player->GetSession()).SendSysMessage("Something went wrong while join queue.");
				
				player->CLOSE_GOSSIP_MENU();
				return true;
				}
				break;
			}

		case GOSSIP_1v1_STATS: // Get Statistics for 1v1
			{
				ArenaTeam* at = sArenaTeamMgr->GetArenaTeamById(player->GetArenaTeamId(ArenaTeam::GetSlotByType(ARENA_TEAM_1v1)));
				if(at)
				{
					std::stringstream s;
					s << "\nRival Champion Rank: " << at->GetStats().Rank;
					s << "\nRival Rating: " << at->GetStats().Rating;
                    s << "\nTotal Winning Brawls: " << at->GetStats().WeekWins;
					s << "\nTotal Brawls Started: " << at->GetStats().WeekGames;
					// s << "\nSeason Games: " << at->GetStats().SeasonGames;
					// s << "\nSeason Wins: " << at->GetStats().SeasonWins;
				
					ChatHandler(player->GetSession()).PSendSysMessage(s.str().c_str());
				
					player->CLOSE_GOSSIP_MENU();
				    return true;
				}
				break;
			}

		// 2v2s gossip functions

        case GOSSIP_2v2s_CREATE_TEAM: // Create new 2v2 solo queue arena team
			{
				if(sWorld->getIntConfig(CONFIG_ARENA_1V1_MIN_LEVEL) <= player->getLevel())
				{
					if(player->GetMoney() >= sWorld->getIntConfig(CONFIG_ARENA_1V1_COSTS) && CreateArenateam2v2s(player, me))
						player->ModifyMoney(sWorld->getIntConfig(CONFIG_ARENA_1V1_COSTS) * -1);
					    player->CLOSE_GOSSIP_MENU();
				}
				else
				{
					ChatHandler(player->GetSession()).PSendSysMessage("You need level %u+ to create an 2v2 solo queue team", sWorld->getIntConfig(CONFIG_ARENA_1V1_MIN_LEVEL));
					player->CLOSE_GOSSIP_MENU();
					return true;
				}
				break;
			}

		case GOSSIP_2v2s_JOIN_UNRATED: // Join 2v2 solo Queue Arena (unrated)
			{
				{
				if(JoinQueueArena2v2s(player, me, false) == false)
					ChatHandler(player->GetSession()).SendSysMessage("Something went wrong while join queue.");
				
				player->CLOSE_GOSSIP_MENU();
				return true;
				}
				break;

			}

		case GOSSIP_2v2s_JOIN_RANKED: // Join 2v2 solo Queue Arena (rated)
			{
				{
				if(Arena1v1CheckTalents(player) && JoinQueueArena2v2s(player, me, true) == false)
					ChatHandler(player->GetSession()).SendSysMessage("Something went wrong while join queue.");
				
				player->CLOSE_GOSSIP_MENU();
				return true;
				}
				break;

			}

	    //3v3 solo queue system functions
		case GOSSIP_3v3s_CREATE_TEAM: // Create new 3v3s team
			{
				if(sWorld->getIntConfig(CONFIG_ARENA_1V1_MIN_LEVEL) <= player->getLevel())
				{
					if(player->GetMoney() >= sWorld->getIntConfig(CONFIG_ARENA_1V1_COSTS) && CreateArenateam3v3s(player, me))
						player->ModifyMoney(sWorld->getIntConfig(CONFIG_ARENA_1V1_COSTS) * -1);
					    player->CLOSE_GOSSIP_MENU();
				}
				else
				{
					ChatHandler(player->GetSession()).PSendSysMessage("You need level %u+ to create an 2v2 solo queue team", sWorld->getIntConfig(CONFIG_ARENA_1V1_MIN_LEVEL));
					player->CLOSE_GOSSIP_MENU();
					return true;
				}
				break;
			}

		case GOSSIP_3v3s_JOIN_UNRATED: // Join 3v3 solo Queue Arena (unrated)
			{
				{
				if(JoinQueueArena3v3s(player, me, false) == false)
					ChatHandler(player->GetSession()).SendSysMessage("Something went wrong while join queue.");
				
				player->CLOSE_GOSSIP_MENU();
				return true;
				}
				break;

			} 	

        //5v5 solo queue system functions
		case GOSSIP_5v5s_JOIN_UNRATED: // Join 3v3 solo Queue Arena (unrated)
			{
				{
				if(JoinQueueArena5v5s(player, me, false) == false)
					ChatHandler(player->GetSession()).SendSysMessage("Something went wrong while join queue.");
				
				player->CLOSE_GOSSIP_MENU();
				return true;
				}
				break;

			} 	

		}

		return true;
		}
	
};

void AddSC_npc_1v1arena()
{
    new npc_1v1arena();
}