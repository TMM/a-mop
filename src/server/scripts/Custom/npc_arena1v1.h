
#ifndef ARENA_1V1_H
#define ARENA_1V1_H

static bool Arena1v1CheckTalents(Player* player)
{
	 // Prot Warr
		if (player->GetSpecializationId(player->GetActiveSpec()) == SPEC_WARRIOR_PROTECTION)
		{
		ChatHandler(player->GetSession()).SendSysMessage("You can't join Brawls, since you are Protection (Tank). Change your specialization to join a Brawl.");
		return false;
		}
	// Mistweaver
		if (player->GetSpecializationId(player->GetActiveSpec()) == SPEC_MONK_MISTWEAVER)
		{
		ChatHandler(player->GetSession()).SendSysMessage("You can't join Brawls, since you are a Mistweaver (Healer). Change your specialization to join a Brawl.");
		return false;
		}
	// DK Blood
		if (player->GetSpecializationId(player->GetActiveSpec()) == SPEC_DK_BLOOD)

		{
		ChatHandler(player->GetSession()).SendSysMessage("You can't join Brawls, since you are Blood (Tank). Change your specialization to join a Brawl.");
		return false;
		}

		if (player->GetSpecializationId(player->GetActiveSpec()) == SPEC_PRIEST_HOLY)

		//if(player->GetSpecializationId(player->GetActiveSpec() == SPEC_WARRIOR_PROTECTION || SPEC_MONK_MISTWEAVER || SPEC_DK_BLOOD || SPEC_PRIEST_HOLY || SPEC_PRIEST_DISCIPLINE || SPEC_PALADIN_PROTECTION || SPEC_PALADIN_HOLY || SPEC_SHAMAN_RESTORATION || SPEC_DROOD_RESTORATION || SPEC_DROOD_BEAR || SPEC_MONK_BREWMASTER))
		{
		ChatHandler(player->GetSession()).SendSysMessage("You can't join Brawls, since you are Holy (Priest). Change your specialization to join a Brawl.");
		return false;
		}

		if (player->GetSpecializationId(player->GetActiveSpec()) == SPEC_PRIEST_DISCIPLINE)

		//if(player->GetSpecializationId(player->GetActiveSpec() == SPEC_WARRIOR_PROTECTION || SPEC_MONK_MISTWEAVER || SPEC_DK_BLOOD || SPEC_PRIEST_HOLY || SPEC_PRIEST_DISCIPLINE || SPEC_PALADIN_PROTECTION || SPEC_PALADIN_HOLY || SPEC_SHAMAN_RESTORATION || SPEC_DROOD_RESTORATION || SPEC_DROOD_BEAR || SPEC_MONK_BREWMASTER))
		{
		ChatHandler(player->GetSession()).SendSysMessage("You can't join Brawls, since you are Discipline (Healer). Change your specialization to join a Brawl.");
		return false;
		}

		if (player->GetSpecializationId(player->GetActiveSpec()) == SPEC_PALADIN_PROTECTION)

		//if(player->GetSpecializationId(player->GetActiveSpec() == SPEC_WARRIOR_PROTECTION || SPEC_MONK_MISTWEAVER || SPEC_DK_BLOOD || SPEC_PRIEST_HOLY || SPEC_PRIEST_DISCIPLINE || SPEC_PALADIN_PROTECTION || SPEC_PALADIN_HOLY || SPEC_SHAMAN_RESTORATION || SPEC_DROOD_RESTORATION || SPEC_DROOD_BEAR || SPEC_MONK_BREWMASTER))
		{
		ChatHandler(player->GetSession()).SendSysMessage("You can't join Brawls, since you are Protection (Tank). Change your specialization to join a Brawl.");
		return false;
		}

		if (player->GetSpecializationId(player->GetActiveSpec()) == SPEC_PALADIN_HOLY)

		//if(player->GetSpecializationId(player->GetActiveSpec() == SPEC_WARRIOR_PROTECTION || SPEC_MONK_MISTWEAVER || SPEC_DK_BLOOD || SPEC_PRIEST_HOLY || SPEC_PRIEST_DISCIPLINE || SPEC_PALADIN_PROTECTION || SPEC_PALADIN_HOLY || SPEC_SHAMAN_RESTORATION || SPEC_DROOD_RESTORATION || SPEC_DROOD_BEAR || SPEC_MONK_BREWMASTER))
		{
		ChatHandler(player->GetSession()).SendSysMessage("You can't join Brawls, since you are Holy (Healer). Change your specialization to join a Brawl.");
		return false;
		}
		
		if (player->GetSpecializationId(player->GetActiveSpec()) == SPEC_SHAMAN_RESTORATION)

		//if(player->GetSpecializationId(player->GetActiveSpec() == SPEC_WARRIOR_PROTECTION || SPEC_MONK_MISTWEAVER || SPEC_DK_BLOOD || SPEC_PRIEST_HOLY || SPEC_PRIEST_DISCIPLINE || SPEC_PALADIN_PROTECTION || SPEC_PALADIN_HOLY || SPEC_SHAMAN_RESTORATION || SPEC_DROOD_RESTORATION || SPEC_DROOD_BEAR || SPEC_MONK_BREWMASTER))
		{
		ChatHandler(player->GetSession()).SendSysMessage("You can't join Brawls, since you are Restoration (Healer). Change your specialization to join a Brawl.");
		return false;
		}

		if (player->GetSpecializationId(player->GetActiveSpec()) == SPEC_DROOD_RESTORATION)
		//if(player->GetSpecializationId(player->GetActiveSpec() == SPEC_WARRIOR_PROTECTION || SPEC_MONK_MISTWEAVER || SPEC_DK_BLOOD || SPEC_PRIEST_HOLY || SPEC_PRIEST_DISCIPLINE || SPEC_PALADIN_PROTECTION || SPEC_PALADIN_HOLY || SPEC_SHAMAN_RESTORATION || SPEC_DROOD_RESTORATION || SPEC_DROOD_BEAR || SPEC_MONK_BREWMASTER))
		{
		ChatHandler(player->GetSession()).SendSysMessage("You can't join Brawls, since you are Restoration (Healer). Change your specialization to join a Brawl.");
		return false;
		}

		if (player->GetSpecializationId(player->GetActiveSpec()) == SPEC_DROOD_BEAR)
      	{
		ChatHandler(player->GetSession()).SendSysMessage("You can't join Brawls, since you are Feral (Tank). Change your specialization to join a Brawl.");
		return false;
		}

		if (player->GetSpecializationId(player->GetActiveSpec()) == SPEC_MONK_BREWMASTER)
	    {
		ChatHandler(player->GetSession()).SendSysMessage("You can't join Brawls, since you are a Brewmaster (Tank). Change your specialization to join a Brawl.");
		return false;
		}

		else

		return true;
}

#endif