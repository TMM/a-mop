/*
<--------------------------------------------------------------------------->
 - Developer(s): Styler, Ghostcrawler336
 - Complete: 90%
 - ScriptName: 'vipcommands' 
 - Comment: VIP commands for VIPS -> GM rank 1+
<--------------------------------------------------------------------------->
*/

#include "World.h"
#include "Player.h"
#include "Language.h"
#include "ScriptMgr.h"
#include "Chat.h"
#include "Group.h"
#include "MapManager.h"

#define MSG_DEFAULT "|cffff6060[Arena-Source VIP]:|r "

static std::string GetTimeString(uint64 time)
{
	uint64 days = time / DAY, hours = (time % DAY) / HOUR, minute = (time % HOUR) / MINUTE;
	std::ostringstream ss;
	if (days)
		ss << days << "d ";
	if (hours)
		ss << hours << "h ";
	ss << minute << 'm';
	return ss.str();
}


class vipcommands : public CommandScript
{
public:
    vipcommands() : CommandScript("vipcommands") { }

    ChatCommand* GetCommands() const
    {
        static ChatCommand vipCommandTable[] =
        {
			{ "mall",          	 	 SEC_VIP,  false, &HandleVipMallCommand,         "", NULL },
			{ "arena",				 SEC_VIP,  false, &HandleArenaCommand,			"", NULL },
			{"talent",				 SEC_VIP,  false, &HandleVipTalentResetCommand,	"", NULL },
			{ "changerace",          SEC_VIP,  false, &HandleChangeRaceCommand,      "", NULL },
			{ "changefaction",		 SEC_VIP,  false, &HandleChangeFactionCommand,	"", NULL },
			{ "customize",			 SEC_VIP,  false, &HandleCustomizeCommand,		"", NULL },
            { NULL,					 0,           false, NULL,                          "", NULL }
        };
		
		static ChatCommand vipInit[] =
		{
			{"vip", SEC_VIP, false, NULL, "", vipCommandTable},
			{ NULL, 0,          false, NULL, "", NULL        }
		};
		return vipInit;
    }
	static bool HandleArenaCommand(ChatHandler* handler, const char* /*args*/)
    {
		BattlegroundTypeId bgTypeId = BATTLEGROUND_AA;
		handler->GetSession()->SendBattleGroundList(handler->GetSession()->GetPlayer()->GetGUID(), bgTypeId);
        return true;
    }

	static bool HandleVipTalentResetCommand(ChatHandler * handler, const char *  /*args*/)
	{
		handler->GetSession()->GetPlayer()->ResetTalents(true);
		handler->GetSession()->GetPlayer()->SendTalentsInfoData(false);
		handler->PSendSysMessage(MSG_DEFAULT"You have reset your talents.");
		return true;
	}

	static bool HandlevipCommand(ChatHandler* handler, const char* args)
    {

        Player* me = handler->GetSession()->GetPlayer();

            me->Say("vip command?", LANG_UNIVERSAL);
            return true;
    }


	/* The commands */

	static bool HandleChangeRaceCommand(ChatHandler* handler, const char* args)
    {

        Player* me = handler->GetSession()->GetPlayer();
		me->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
		handler->PSendSysMessage("Relog to change race of your character.");
        return true;
    }

	static bool HandleChangeFactionCommand(ChatHandler* handler, const char* args)
    {

        Player* me = handler->GetSession()->GetPlayer();
		me->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
		handler->PSendSysMessage("Relog to change faction of your character.");
        return true;
    }

	/*static bool HandleMaxSkillsCommand(ChatHandler* handler, const char* args)
    {

        Player* me = handler->GetSession()->GetPlayer();
		me->UpdateSkillsForLevel();
		handler->PSendSysMessage("Your weapon skills are now maximized.");
        return true;
    }*/ // depricated

	static bool HandleCustomizeCommand(ChatHandler* handler, const char* args)
    {

        Player* me = handler->GetSession()->GetPlayer();
		me->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
		handler->PSendSysMessage("Relog to customize your character.");
        return true;
    }

	static bool HandleVipMallCommand(ChatHandler* handler, const char* args)
    {

            Player* me = handler->GetSession()->GetPlayer();
		
		if (me->GetZoneId() == 5287)
		{
		handler->PSendSysMessage("Not allowed in Gurubashi");
		return false;
		}
         if (me->isInCombat())
        {
            handler->SendSysMessage(LANG_YOU_IN_COMBAT);
            handler->SetSentErrorMessage(true);
            return false;
        }

        // stop flight if need
        if (me->isInFlight())
        {
            me->GetMotionMaster()->MovementExpired();
            me->CleanupAfterTaxiFlight();
        }
        // stop flight if need
        if (me->isInFlight())
        {
            me->GetMotionMaster()->MovementExpired();
            me->CleanupAfterTaxiFlight();
        }
        // save only in non-flight case
        else
            me->SaveRecallPosition();
			me->TeleportTo(870, 6596.368652f, 6101.055664f, 211.057999f, 3.612347f); // MapId, X, Y, Z, O
			handler->PSendSysMessage("|cffff6060[Information]:|r You Have Been Teleported to the VIP Mall!");
			return true;
			
    }
	    
};

void AddSC_vipcommands()
{
    new vipcommands();
}