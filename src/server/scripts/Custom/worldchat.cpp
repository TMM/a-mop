/*
<--------------------------------------------------------------------------->
- Developer(s): Ghostcrawler336
- Complete: 100%
- ScriptName: 'World Chat Command'
- Comment: Untested
<--------------------------------------------------------------------------->
*/
 
#include "ScriptPCH.h"
#include "Chat.h"
#include "Common.h"
 
/* Colors */
#define MSG_COLOR_ORANGE "|cffFFA500"
#define MSG_COLOR_DARKORANGE "|cffFF8C00"
#define MSG_COLOR_RED "|cffFF0000"
#define MSG_COLOR_LIGHTRED "|cffD63931"
#define MSG_COLOR_ROYALBLUE "|cff4169E1"
#define MSG_COLOR_LIGHTBLUE "|cffADD8E6"
#define MSG_COLOR_YELLOW "|cffFFFF00"
#define MSG_COLOR_GREEN "|cff008000"
#define MSG_COLOR_PURPLE "|cff660066"
#define MSG_COLOR_WHITE "|cffffffff"
#define MSG_COLOR_SUBWHITE "|cffbbbbbb"
#define MSG_COLOR_BLACK "|cff000000"
#define MSG_COLOR_CRIMSON "|cffDC143C"
#define MSG_COLOR_BVIOLET "|cff8A2BE2"
#define MSG_COLOR_DGREY "|cff262d35"
#define MSG_COLOR_SALMON "|cffE9967A"


 
class World_Chat : public CommandScript
{
public:
World_Chat() : CommandScript("World_Chat") { }
 
static bool HandleWorldChatCommand(ChatHandler * pChat, const char * msg)
{
if(!*msg)
return false;

 if (!pChat->GetSession()->GetPlayer()->CanSpeak())
                        return false;
 
Player * player = pChat->GetSession()->GetPlayer();
char message[1024];
 
switch(player->GetSession()->GetSecurity())
{
case SEC_PLAYER:

snprintf(message, 1024, "[World][Player][%s%s|r]: %s%s|r", MSG_COLOR_WHITE, player->GetName(), MSG_COLOR_LIGHTBLUE, msg);
sWorld->SendGlobalText(message, NULL);
break;


// RANK 2
case SEC_VIP:
snprintf(message, 1024, "[World][VIP][%s%s|r]: %s%s|r", MSG_COLOR_GREEN, player->GetName(), MSG_COLOR_LIGHTBLUE, msg);
sWorld->SendGlobalText(message, NULL);
break;


// RANK 3
case SEC_TRIAL_GM:
snprintf(message, 1024, "[World][Trial GM][%s%s|r]: %s%s|r", MSG_COLOR_GREEN, player->GetName(), MSG_COLOR_LIGHTBLUE, msg);
sWorld->SendGlobalText(message, NULL);
break;

// RANK 4
case SEC_DEVELOPER:
snprintf(message, 1024, "[World][Developer][%s%s|r]: %s%s|r", MSG_COLOR_RED, player->GetName(), MSG_COLOR_LIGHTBLUE, msg);
sWorld->SendGlobalText(message, NULL);
break;

// RANK 5
case SEC_GAMEMASTER:
snprintf(message, 1024, "[World][GM][%s%s|r]: %s%s|r", MSG_COLOR_ROYALBLUE, player->GetName(), MSG_COLOR_LIGHTBLUE, msg);
sWorld->SendGlobalText(message, NULL);
break;

// RANK 6
case SEC_MANAGER:
snprintf(message, 1024, "[World][Manager][%s%s|r]: %s%s|r", MSG_COLOR_LIGHTRED, player->GetName(), MSG_COLOR_LIGHTBLUE, msg);
sWorld->SendGlobalText(message, NULL);
break;

// RANK 7
case SEC_ADMINISTRATOR:
snprintf(message, 1024, "[World][Admin][%s%s|r]: %s%s|r", MSG_COLOR_CRIMSON, player->GetName(), MSG_COLOR_BVIOLET, msg);
sWorld->SendGlobalText(message, NULL);
break;

// RANK 8
case SEC_OWNER:
snprintf(message, 1024, "[World][Owner][%s%s|r]: %s%s|r", MSG_COLOR_DGREY, player->GetName(), MSG_COLOR_DARKORANGE, msg);
sWorld->SendGlobalText(message, NULL);
break;



 
}
return true;
}
 
ChatCommand * GetCommands() const
{
static ChatCommand HandleWorldChatCommandTable[] =
{
{ "world", SEC_PLAYER, true, &HandleWorldChatCommand, "", NULL },
{ NULL, 0, false, NULL, "", NULL }
};
return HandleWorldChatCommandTable;
}
};
 
void AddSC_World_Chat()
{
new World_Chat;
}