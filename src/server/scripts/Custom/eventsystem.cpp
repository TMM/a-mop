#include "ScriptMgr.h"
#include "Language.h"
#include "Chat.h"
#include "ObjectMgr.h"
<<<<<<< .merge_file_a08420
=======
#include "Player.h"
>>>>>>> .merge_file_a09852
 
class eventcommands : public CommandScript
{
public:
    eventcommands() : CommandScript("eventcommands") { }
 
    ChatCommand* GetCommands() const
    {
        static ChatCommand eventCommandTable[] =
        {
            { "schedule",     SEC_MODERATOR,  true, &HandleEventSchedule,         "", NULL },   // DONE: Schedules an Event
            { "start",        SEC_MODERATOR,  true, &HandleEventStart,            "", NULL },   // DONE: Starts an Event based on ID
            { "finish",       SEC_MODERATOR,  true, &HandleEventFinish,           "", NULL },   // DONE: Finishes an Event based on ID
            { "reward",       SEC_MODERATOR,  true, &HandleEventReward,           "", NULL },   // DONE: Reward player with Event Item
            { "remove",       SEC_MODERATOR,  true, &HandleEventRewardRemove,     "", NULL },   // DONE: Remove player with an Event Item
            { "spectateon",   SEC_MODERATOR,  true, &HandleEventSpectateOn,       "", NULL },   // DONE: Allow players to spectate the event
            { "spectateoff",  SEC_MODERATOR,  true, &HandleEventSpectateOff,      "", NULL },   // DONE: Prohibit players to spectate the event
           
            //{ "spectateset",  SEC_MODERATOR,  true, &HandleEventSpectateSet,      "", NULL },   // TODO: Set the location GPS for spectators to spawn @ event.
            { "port",         SEC_MODERATOR,  true, &Handleport,                  "", NULL },   // TODO: Port player out of Event
            { NULL,             0,                     false, NULL,               "", NULL }
        };
        static ChatCommand commandTable[] =
        {
            { "event", SEC_GAMEMASTER, false, NULL, "", eventCommandTable }, 
            { NULL, 0, false, NULL, "", NULL }
        };
        return commandTable;
    }
    
    static bool HandleEventSchedule(ChatHandler* handler, const char* args) // Schedule an Event
    {
        // Example of command being used
        //  .event schedule HnS 30m Cakes
        //  .event schedule [EVENT] [TIME] [HOST]

        // Take into account all variables first
        if (!*args)
            return false;

        // Event Name
        char* eventStr = strtok((char*)args, " ");
        if (!eventStr)
            return false;

        // Time in Minutes till Event
        char* timeStr = strtok((char*)NULL, " ");
        if (!timeStr)
            return false;

        if (timeStr < 0) // Check for a number
            return false;

<<<<<<< .merge_file_a08420
        // GM/Host name of Event
        char* hostStr = strtok((char*)NULL, " ");
=======
        Player* pPlayer = handler->GetSession()->GetPlayer();
        // GM/Host name of Event
        const char* hostStr = pPlayer->GetName();
>>>>>>> .merge_file_a09852
        if (!hostStr)
            return false;

        // GMs can't schedule more than one event until it's been completed
        QueryResult viewid = CharacterDatabase.PQuery("SELECT `id` FROM `events` WHERE `host`='%s' && `completed`='0'", hostStr);
        if (viewid)
            return false;

        // Defining Prizes (50%)

        /*

        char* firstStr = strtok((char*)NULL, " ");
        if (!firstStr)
            return false;

        char* secondStr = strtok((char*)NULL, " ");
        if (!secondStr)
            return false;

        char* thirdStr = strtok((char*)NULL, " ");
        if (!thirdStr)
            return false;

        */

        std::string argStr = (char*)args;

        if (argStr == "HnS")
        {
            char* eventCode = "Hide and Seek Event";
            
            // Send Event Announcement
            // Time Formatting - NEEDS WORK
            int32 time = atoi(timeStr);
            if (time > 0)
                sWorld->SendWorldText(LANG_ANNOUNCE_EVENT_MINUTES, eventCode, timeStr); 

            // Add event into DB
            QueryResult result = CharacterDatabase.PQuery("INSERT INTO `events` (`id`, `eventname`, `time`, `1stprize`, `2ndprize`, `3rdprize`, `host`) VALUES ('', '%s', '%s', 'TBA', 'TBA', 'TBA', '%s')", eventCode, timeStr, hostStr);
            
            // Show the GM their event ID just created
            QueryResult viewid = CharacterDatabase.PQuery("SELECT `id`, `host`, `completed` FROM events WHERE `host` = '%s' && `completed` = '0'", hostStr);
            if (!viewid)
                return false;

            if (viewid) 
            {
                Field * field = viewid->Fetch();
                uint32 eventid = field[0].GetUInt32();
                std::stringstream buf;
                buf << "Key ID: " << "|cFF1E90FF" << eventid << "|r";
                handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00You have scheduled an event in %s minutes, with the |r %s. |cffffcc00NOTE: Remember this key in order to manage it. |r", timeStr, buf.str());
                    return true;
            }
        }

        if (argStr == "1v1")
        {
            char* eventCode = "1v1 Tournament Event";
            
            // Send Event Announcement
            // Time Formatting - NEEDS WORK
            int32 time = atoi(timeStr);
            if (time > 0)
                sWorld->SendWorldText(LANG_ANNOUNCE_EVENT_MINUTES, eventCode, timeStr); 

            // Add event into DB
            QueryResult result = CharacterDatabase.PQuery("INSERT INTO `events` (`id`, `eventname`, `time`, `1stprize`, `2ndprize`, `3rdprize`, `host`) VALUES ('', '%s', '%s', 'TBA', 'TBA', 'TBA', '%s')", eventCode, timeStr, hostStr);
            
            // Show the GM their event ID just created
            QueryResult viewid = CharacterDatabase.PQuery("SELECT `id`, `host`, `completed` FROM events WHERE `host` = '%s' && `completed` = '0'", hostStr);
            if (!viewid)
                return false;

            if (viewid) 
            {
                Field * field = viewid->Fetch();
                uint32 eventid = field[0].GetUInt32();
                std::stringstream buf;
                buf << "Key ID: " << "|cFF1E90FF" << eventid << "|r";
                handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00You have scheduled an event in %s minutes, with the |r %s. |cffffcc00NOTE: Remember this key in order to manage it. |r", timeStr, buf.str());
                    return true;
            }
        }

        if (argStr == "2v2")
        {
            char* eventCode = "2v2 Tournament Event";
            
            // Send Event Announcement
            // Time Formatting - NEEDS WORK
            int32 time = atoi(timeStr);
            if (time > 0)
                sWorld->SendWorldText(LANG_ANNOUNCE_EVENT_MINUTES, eventCode, timeStr); 

            // Add event into DB
            QueryResult result = CharacterDatabase.PQuery("INSERT INTO `events` (`id`, `eventname`, `time`, `1stprize`, `2ndprize`, `3rdprize`, `host`) VALUES ('', '%s', '%s', 'TBA', 'TBA', 'TBA', '%s')", eventCode, timeStr, hostStr);
            
            // Show the GM their event ID just created
            QueryResult viewid = CharacterDatabase.PQuery("SELECT `id`, `host`, `completed` FROM events WHERE `host` = '%s' && `completed` = '0'", hostStr);
            if (!viewid)
                return false;

            if (viewid) 
            {
                Field * field = viewid->Fetch();
                uint32 eventid = field[0].GetUInt32();
                std::stringstream buf;
                buf << "Key ID: " << "|cFF1E90FF" << eventid << "|r";
                handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00You have scheduled an event in %s minutes, with the |r %s. |cffffcc00NOTE: Remember this key in order to manage it. |r", timeStr, buf.str());
                    return true;
            }
        }

        if (argStr == "3v3")
        {
            char* eventCode = "3v3 Tournament Event";
            
            // Send Event Announcement
            // Time Formatting - NEEDS WORK
            int32 time = atoi(timeStr);
            if (time > 0)
                sWorld->SendWorldText(LANG_ANNOUNCE_EVENT_MINUTES, eventCode, timeStr); 

            // Add event into DB
            QueryResult result = CharacterDatabase.PQuery("INSERT INTO `events` (`id`, `eventname`, `time`, `1stprize`, `2ndprize`, `3rdprize`, `host`) VALUES ('', '%s', '%s', 'TBA', 'TBA', 'TBA', '%s')", eventCode, timeStr, hostStr);
            
            // Show the GM their event ID just created
            QueryResult viewid = CharacterDatabase.PQuery("SELECT `id`, `host`, `completed` FROM events WHERE `host` = '%s' && `completed` = '0'", hostStr);
            if (!viewid)
                return false;

            if (viewid) 
            {
                Field * field = viewid->Fetch();
                uint32 eventid = field[0].GetUInt32();
                std::stringstream buf;
                buf << "Key ID: " << "|cFF1E90FF" << eventid << "|r";
                handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00You have scheduled an event in %s minutes, with the |r %s. |cffffcc00NOTE: Remember this key in order to manage it. |r", timeStr, buf.str());
                    return true;
            }
        }

        if (argStr == "5v5")
        {
            char* eventCode = "5v5 Tournament Event";
            
            // Send Event Announcement
            // Time Formatting - NEEDS WORK
            int32 time = atoi(timeStr);
            if (time > 0)
                sWorld->SendWorldText(LANG_ANNOUNCE_EVENT_MINUTES, eventCode, timeStr); 

            // Add event into DB
            QueryResult result = CharacterDatabase.PQuery("INSERT INTO `events` (`id`, `eventname`, `time`, `1stprize`, `2ndprize`, `3rdprize`, `host`) VALUES ('', '%s', '%s', 'TBA', 'TBA', 'TBA', '%s')", eventCode, timeStr, hostStr);
            
            // Show the GM their event ID just created
            QueryResult viewid = CharacterDatabase.PQuery("SELECT `id`, `host`, `completed` FROM events WHERE `host` = '%s' && `completed` = '0'", hostStr);
            if (!viewid)
                return false;

            if (viewid) 
            {
                Field * field = viewid->Fetch();
                uint32 eventid = field[0].GetUInt32();
                std::stringstream buf;
                buf << "Key ID: " << "|cFF1E90FF" << eventid << "|r";
                handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00You have scheduled an event in %s minutes, with the |r %s. |cffffcc00NOTE: Remember this key in order to manage it. |r", timeStr, buf.str());
                    return true;
            }
        }

        if (argStr == "10v10")
        {
            char* eventCode = "10v10 Tournament Event";
            
            // Send Event Announcement
            // Time Formatting - NEEDS WORK
            int32 time = atoi(timeStr);
            if (time > 0)
                sWorld->SendWorldText(LANG_ANNOUNCE_EVENT_MINUTES, eventCode, timeStr); 

            // Add event into DB
            QueryResult result = CharacterDatabase.PQuery("INSERT INTO `events` (`id`, `eventname`, `time`, `1stprize`, `2ndprize`, `3rdprize`, `host`) VALUES ('', '%s', '%s', 'TBA', 'TBA', 'TBA', '%s')", eventCode, timeStr, hostStr);
            
            // Show the GM their event ID just created
            QueryResult viewid = CharacterDatabase.PQuery("SELECT `id`, `host`, `completed` FROM events WHERE `host` = '%s' && `completed` = '0'", hostStr);
            if (!viewid)
                return false;

            if (viewid) 
            {
                Field * field = viewid->Fetch();
                uint32 eventid = field[0].GetUInt32();
                std::stringstream buf;
                buf << "Key ID: " << "|cFF1E90FF" << eventid << "|r";
                handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00You have scheduled an event in %s minutes, with the |r %s. |cffffcc00NOTE: Remember this key in order to manage it. |r", timeStr, buf.str());
                return true;
            }
        }

<<<<<<< .merge_file_a08420
=======
        if (argStr == "TRIV")
        {
            char* eventCode = "Trivia Event";
            
            // Send Event Announcement
            // Time Formatting - NEEDS WORK
            int32 time = atoi(timeStr);
            if (time > 0)
                sWorld->SendWorldText(LANG_ANNOUNCE_EVENT_MINUTES, eventCode, timeStr); 

            // Add event into DB
            QueryResult result = CharacterDatabase.PQuery("INSERT INTO `events` (`id`, `eventname`, `time`, `1stprize`, `2ndprize`, `3rdprize`, `host`) VALUES ('', '%s', '%s', 'TBA', 'TBA', 'TBA', '%s')", eventCode, timeStr, hostStr);
            
            // Show the GM their event ID just created
            QueryResult viewid = CharacterDatabase.PQuery("SELECT `id`, `host`, `completed` FROM events WHERE `host` = '%s' && `completed` = '0'", hostStr);
            if (!viewid)
                return false;

            if (viewid) 
            {
                Field * field = viewid->Fetch();
                uint32 eventid = field[0].GetUInt32();
                std::stringstream buf;
                buf << "Key ID: " << "|cFF1E90FF" << eventid << "|r";
                handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00You have scheduled an event in %s minutes, with the |r %s. |cffffcc00NOTE: Remember this key in order to manage it. |r", timeStr, buf.str());
                    return true;
            }
        }
        if (argStr == "FLYDIE")
        {
            char* eventCode = "Fly or Die Event";
            
            // Send Event Announcement
            // Time Formatting - NEEDS WORK
            int32 time = atoi(timeStr);
            if (time > 0)
                sWorld->SendWorldText(LANG_ANNOUNCE_EVENT_MINUTES, eventCode, timeStr); 

            // Add event into DB
            QueryResult result = CharacterDatabase.PQuery("INSERT INTO `events` (`id`, `eventname`, `time`, `1stprize`, `2ndprize`, `3rdprize`, `host`) VALUES ('', '%s', '%s', 'TBA', 'TBA', 'TBA', '%s')", eventCode, timeStr, hostStr);
            
            // Show the GM their event ID just created
            QueryResult viewid = CharacterDatabase.PQuery("SELECT `id`, `host`, `completed` FROM events WHERE `host` = '%s' && `completed` = '0'", hostStr);
            if (!viewid)
                return false;

            if (viewid) 
            {
                Field * field = viewid->Fetch();
                uint32 eventid = field[0].GetUInt32();
                std::stringstream buf;
                buf << "Key ID: " << "|cFF1E90FF" << eventid << "|r";
                handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00You have scheduled an event in %s minutes, with the |r %s. |cffffcc00NOTE: Remember this key in order to manage it. |r", timeStr, buf.str());
                    return true;
            }
        }
        if (argStr == "KILLGM")
        {
            char* eventCode = "Kill the GM Event";
            
            // Send Event Announcement
            // Time Formatting - NEEDS WORK
            int32 time = atoi(timeStr);
            if (time > 0)
                sWorld->SendWorldText(LANG_ANNOUNCE_EVENT_MINUTES, eventCode, timeStr); 

            // Add event into DB
            QueryResult result = CharacterDatabase.PQuery("INSERT INTO `events` (`id`, `eventname`, `time`, `1stprize`, `2ndprize`, `3rdprize`, `host`) VALUES ('', '%s', '%s', 'TBA', 'TBA', 'TBA', '%s')", eventCode, timeStr, hostStr);
            
            // Show the GM their event ID just created
            QueryResult viewid = CharacterDatabase.PQuery("SELECT `id`, `host`, `completed` FROM events WHERE `host` = '%s' && `completed` = '0'", hostStr);
            if (!viewid)
                return false;

            if (viewid) 
            {
                Field * field = viewid->Fetch();
                uint32 eventid = field[0].GetUInt32();
                std::stringstream buf;
                buf << "Key ID: " << "|cFF1E90FF" << eventid << "|r";
                handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00You have scheduled an event in %s minutes, with the |r %s. |cffffcc00NOTE: Remember this key in order to manage it. |r", timeStr, buf.str());
                    return true;
            }
        }
        if (argStr == "SCAV")
        {
            char* eventCode = "Scavenger Hunt Event";
            
            // Send Event Announcement
            // Time Formatting - NEEDS WORK
            int32 time = atoi(timeStr);
            if (time > 0)
                sWorld->SendWorldText(LANG_ANNOUNCE_EVENT_MINUTES, eventCode, timeStr); 

            // Add event into DB
            QueryResult result = CharacterDatabase.PQuery("INSERT INTO `events` (`id`, `eventname`, `time`, `1stprize`, `2ndprize`, `3rdprize`, `host`) VALUES ('', '%s', '%s', 'TBA', 'TBA', 'TBA', '%s')", eventCode, timeStr, hostStr);
            
            // Show the GM their event ID just created
            QueryResult viewid = CharacterDatabase.PQuery("SELECT `id`, `host`, `completed` FROM events WHERE `host` = '%s' && `completed` = '0'", hostStr);
            if (!viewid)
                return false;

            if (viewid) 
            {
                Field * field = viewid->Fetch();
                uint32 eventid = field[0].GetUInt32();
                std::stringstream buf;
                buf << "Key ID: " << "|cFF1E90FF" << eventid << "|r";
                handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00You have scheduled an event in %s minutes, with the |r %s. |cffffcc00NOTE: Remember this key in order to manage it. |r", timeStr, buf.str());
                    return true;
            }
        }
        if (argStr == "STAIR")
        {
            char* eventCode = "Staircase Event";
            
            // Send Event Announcement
            // Time Formatting - NEEDS WORK
            int32 time = atoi(timeStr);
            if (time > 0)
                sWorld->SendWorldText(LANG_ANNOUNCE_EVENT_MINUTES, eventCode, timeStr); 

            // Add event into DB
            QueryResult result = CharacterDatabase.PQuery("INSERT INTO `events` (`id`, `eventname`, `time`, `1stprize`, `2ndprize`, `3rdprize`, `host`) VALUES ('', '%s', '%s', 'TBA', 'TBA', 'TBA', '%s')", eventCode, timeStr, hostStr);
            
            // Show the GM their event ID just created
            QueryResult viewid = CharacterDatabase.PQuery("SELECT `id`, `host`, `completed` FROM events WHERE `host` = '%s' && `completed` = '0'", hostStr);
            if (!viewid)
                return false;

            if (viewid) 
            {
                Field * field = viewid->Fetch();
                uint32 eventid = field[0].GetUInt32();
                std::stringstream buf;
                buf << "Key ID: " << "|cFF1E90FF" << eventid << "|r";
                handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00You have scheduled an event in %s minutes, with the |r %s. |cffffcc00NOTE: Remember this key in order to manage it. |r", timeStr, buf.str());
                    return true;
            }
        }
>>>>>>> .merge_file_a09852
        return false;
    }

    // 100% working function
    // .event start [KEYID]
    static bool HandleEventStart(ChatHandler* handler, const char* args) // Schedule an Event
    {
        // .event start [id]
        if (!*args)
            return false;

        // Get ID of Event
        char* idStr = strtok((char*)args, " ");
        if (!idStr)
            return false;

        QueryResult selectid = CharacterDatabase.PQuery("SELECT `id`, `eventname`, `host` FROM events WHERE `id` = '%s' && `started`='0'", idStr);

        // Check if ID actually exists
        if (selectid)
        {
            Field * field = selectid->Fetch();
            uint32 eventid = field[0].GetUInt32();
            std::string eventname = field[1].GetString();
            std::string host = field[2].GetString();

            // Set Event Status to Started

                QueryResult result = CharacterDatabase.PQuery("UPDATE `events` SET `started`='1' WHERE `id`='%s' && `started`='0'", idStr);
                handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00 Event has now been started with|r |cFF1E90FFID %s|r", idStr);
                sWorld->SendWorldText(LANG_ANNOUNCE_STARTED, eventname.c_str(), host.c_str());
                return true;
        }

        return false;
    }
    
    // 100% working function
    // .event finish [KEYID]
    static bool HandleEventFinish(ChatHandler* handler, const char* args) // Finishes an event
    {
        // .event finish [id]
        if (!*args)
            return false;

        // Get ID of Event
        char* idStr = strtok((char*)args, " ");
        if (!idStr)
            return false;

        QueryResult selectid = CharacterDatabase.PQuery("SELECT `id`, `eventname` FROM events WHERE `id` = '%s' && `completed`='0'", idStr);

        // Check if ID actually exists
        if (selectid)
        {
            Field * field = selectid->Fetch();
            uint32 eventid = field[0].GetUInt32();
            std::string eventname = field[1].GetString();

            // Set Event Status to Finished
            // Make sure event has already started before finishing it
                QueryResult result = CharacterDatabase.PQuery("UPDATE `events` SET `completed`='1' WHERE `id`='%s' && `started`='1'", idStr);
                handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00 Event has been ended with|r |cFF1E90FFID %s|r", idStr);
                sWorld->SendWorldText(LANG_ANNOUNCE_FINISHED, eventname.c_str());
                return true;
        }

        return false;
    }


    // EVENT SPECTATOR MODE

    // 100% working function
    // .event spectateon [KEYID]
    static bool HandleEventSpectateOn(ChatHandler* handler, const char* args) // Finishes an event
    {
        // Default spectate is set to off.
        // GMs can allow spectation by doing .event spectateon

        // COORDINATES MUST BE SET BEFORE THIS WITH .EVENT SPECTATE [KEYID] SET
        // ADD A CHECK BEFORE ALLOWING SPECTATORS!

        // .event spectate [KEYID] ON||OFF
        if (!*args)
            return false;

        // Get Key ID of Event
        char* idStr = strtok((char*)args, " ");
        if (!idStr)
            return false;

        QueryResult check = CharacterDatabase.PQuery("SELECT `id`, `eventname` FROM events WHERE `id` = '%s' && `completed`='0' && `spectate`='0' && `x` > '0'", idStr);

        // Check if ID actually exists
        if (check)
        {    

            // Allow Spectators to Event
            // Coordinates for event must be set
            // Just check for X coordinate, not necessary to check for the rest
            QueryResult setOn = CharacterDatabase.PQuery("UPDATE `events` SET `spectate`='1' WHERE `id`='%s' && `completed`='0' && `spectate`='0'", idStr);
            {
                handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00 Event with|r |cFF1E90FFID %s |r|cffffcc00can now be spectated via. the teleporter!|r", idStr);
                return true;
            }

            handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00 Error! Something went wrong :("); // Shouldn't happen
            return false;

        }
        else
        {
            handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00 Error! .event spectate set hasn't been set OR Event Key ID doesn't exist OR The Event is already in spectator mode!");
            return true;
        }
    }

    // 100% working function
    // .event spectateoff [KEYID]
    static bool HandleEventSpectateOff(ChatHandler* handler, const char* args) // Finishes an event
    {
        // Default spectate is set to off.
        // GMs can turn off spectation by doing .event spectateoff

        // Spectation must be on before doing this

        // .event spectateoff [KEYID]
        if (!*args)
            return false;

        // Get Key ID of Event
        char* idStr = strtok((char*)args, " ");
        if (!idStr)
            return false;

        QueryResult check = CharacterDatabase.PQuery("SELECT `id`, `eventname` FROM events WHERE `id` = '%s' && `completed`='0'", idStr);

        // Check if ID actually exists
        if (check)
        {    

            // Prohibit Spectators to Event
            // Spectators must be enabled
            QueryResult setOff = CharacterDatabase.PQuery("UPDATE `events` SET `spectate`='0' WHERE `id`='%s' && `completed`='0' && `spectate`='1'", idStr);
            {
                handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00 Event with|r |cFF1E90FFID %s|r |cffffcc00can no longer be spectated!|r", idStr);
                return true;
            }

            handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00 Error! Event already doesn't allow spectating anyway!");
            return false;

        }
        else
        {
            handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00 Error! That Event is not found, did you forget your Event ID?");
            return true;
        }
    }

    // Not working yet, TO:DO
    // .event spectateoff [KEYID]
    static bool HandleEventSpectateSet(ChatHandler* handler, const char* args) // Finishes an event
    {
       
        // Set the location for where spectators will spawn
        // .event spectateset [KEYID]
        if (!*args)
            return false;

        // Get Key ID of Event
        char* idStr = strtok((char*)args, " ");
        if (!idStr)
            return false;

        QueryResult check = CharacterDatabase.PQuery("SELECT `id`, `eventname` FROM events WHERE `id` = '%s' && `completed`='0'", idStr);
        
        // Check if event exists and not completed
        if (check)
        {
<<<<<<< .merge_file_a08420
         // Get current GPS of GM
        Player* player = handler->GetSession()->GetPlayer();
        
        /*
        char x = player->GetPositionX();
        char y = player->GetPositionY();
        char z = player->GetPositionZ();
        char o = player->GetOrientation();
        char mapid = player->GetMapId();
        */

        QueryResult setspectate = CharacterDatabase.PQuery("UPDATE `events` SET `mapid`='1', `x`='%f', `y`='1', `z`='1', `o`='1' WHERE `id`='%s' && `completed`='0'", player->GetPositionX(), idStr);
        {
            handler->PSendSysMessage("|cff00c78c<GM Notice>|r|cffffcc00 Event with Key|r |cFF1E90FFID %s|r |cffffcc00can has been set with it's new spectator spawn on your current position.|r", idStr);
            return true;
        }
        }
        else
        {
            handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00 Error! That Event is not found, or has already finished!");
=======
            // Get current GPS of GM
            Player* player = handler->GetSession()->GetPlayer();
        
            float x = player->GetPositionX();
            float y = player->GetPositionY();
            float z = player->GetPositionZ();
            float o = player->GetOrientation();
            uint32 mapid = player->GetMapId();

            QueryResult setspectate = CharacterDatabase.PQuery("UPDATE `events` SET `mapid`='%u', `x`='%u', `y`='%u', `z`='%u', `o`='%u' WHERE `id`='13' && `completed`='0'", mapid, x, y,z ,o /*idStr*/);
                {
                   handler->PSendSysMessage("|cff00c78c<GM Notice>|r|cffffcc00 Event with Key|r |cFF1E90FFID %s|r |cffffcc00can has been set with it's new spectator spawn on your current position.|r", idStr);
                   return true;
                }
        }

        else
        {
            handler->PSendSysMessage("|cff00c78c<GM Notice>|r |cffffcc00 Error! That event is not found, or has already finished!");
>>>>>>> .merge_file_a09852
            return true;
        }
        return true;
    }

    // 0% TO:DO 
    static bool Handleport(ChatHandler* handler, const char* args) // Teleports the selected player to stormwind or org based on faction
    {
        return true;
    }

    // 100% Working Function
    // .event reward [ID]
    static bool HandleEventReward(ChatHandler* handler, const char* args)
    {  
        if (!*args)
            return false;

        uint32 itemId = 0;

        if (args[0] == '[')                                        // [name] manual form
        {
            char const* itemNameStr = strtok((char*)args, "]");

            if (itemNameStr && itemNameStr[0])
            {
                std::string itemName = itemNameStr+1;
                WorldDatabase.EscapeString(itemName);

                PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_ITEM_TEMPLATE_BY_NAME);
                stmt->setString(0, itemName);
                PreparedQueryResult result = WorldDatabase.Query(stmt);

                if (!result)
                {
                    handler->PSendSysMessage(LANG_COMMAND_COULDNOTFIND, itemNameStr+1);
                    handler->SetSentErrorMessage(true);
                    return false;
                }
                itemId = result->Fetch()->GetUInt32();
            }
            else
                return false;
        }
        else                                                    // item_id or [name] Shift-click form |color|Hitem:item_id:0:0:0|h[name]|h|r
        {
            char const* id = handler->extractKeyFromLink((char*)args, "Hitem");
            if (!id)
                return false;
            itemId = uint32(atol(id));
        }

        char const* ccount = strtok(NULL, " ");

        int32 count = 1;

        if (ccount)
            count = strtol(ccount, NULL, 10);

        if (count == 0)
            count = 1;

        Player* player = handler->GetSession()->GetPlayer();
        Player* playerTarget = handler->getSelectedPlayer();
        if (!playerTarget)
            playerTarget = player;

        sLog->outDebug(LOG_FILTER_GENERAL, handler->GetTrinityString(LANG_ADDITEM), itemId, count);

        ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(itemId);
        if (!itemTemplate)
        {
            handler->PSendSysMessage(LANG_COMMAND_ITEMIDINVALID, itemId);
            handler->SetSentErrorMessage(true);
            return false;
        }

        // Can't be a negative number
        if (count < 0)
            return false;

        // Adding items
        uint32 noSpaceForCount = 0;

        // check space and find places
        ItemPosCountVec dest;
        InventoryResult msg = playerTarget->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, itemId, count, &noSpaceForCount);
        if (msg != EQUIP_ERR_OK)                               // convert to possible store amount
            count -= noSpaceForCount;

        if (count == 0 || dest.empty())                         // can't add any
        {
            handler->PSendSysMessage(LANG_ITEM_CANNOT_CREATE, itemId, noSpaceForCount);
            handler->SetSentErrorMessage(true);
            return false;
        }

        Item* item = playerTarget->StoreNewItem(dest, itemId, true, Item::GenerateItemRandomPropertyId(itemId));

        if (count > 0 && item)
        {
            player->SendNewItem(item, count, false, true);
            if (player != playerTarget)
                playerTarget->SendNewItem(item, count, true, false);
        }

        if (noSpaceForCount > 0)
            handler->PSendSysMessage(LANG_ITEM_CANNOT_CREATE, itemId, noSpaceForCount);

        playerTarget->SendChatMessage("|cffB400B4You have just been awarded an event item for participating in an event!");
        handler->PSendSysMessage("|cffB400B4You have just rewarded a player with a prize. This command has been logged for future references.");
        return true;
    }

    // 100% Working Function
    // .event remove [ID] -1
    static bool HandleEventRewardRemove(ChatHandler* handler, const char* args)
    {  
        if (!*args)
            return false;

        uint32 itemId = 0;

        if (args[0] == '[')                                        // [name] manual form
        {
            char const* itemNameStr = strtok((char*)args, "]");

            if (itemNameStr && itemNameStr[0])
            {
                std::string itemName = itemNameStr+1;
                WorldDatabase.EscapeString(itemName);

                PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_ITEM_TEMPLATE_BY_NAME);
                stmt->setString(0, itemName);
                PreparedQueryResult result = WorldDatabase.Query(stmt);

                if (!result)
                {
                    handler->PSendSysMessage(LANG_COMMAND_COULDNOTFIND, itemNameStr+1);
                    handler->SetSentErrorMessage(true);
                    return false;
                }
                itemId = result->Fetch()->GetUInt32();
            }
            else
                return false;
        }
        else                                                    // item_id or [name] Shift-click form |color|Hitem:item_id:0:0:0|h[name]|h|r
        {
            char const* id = handler->extractKeyFromLink((char*)args, "Hitem");
            if (!id)
                return false;
            itemId = uint32(atol(id));
        }

        char const* ccount = strtok(NULL, " ");

        int32 count = 1;

        if (ccount)
            count = strtol(ccount, NULL, 10);

        if (count == 0)
            count = 1;

        Player* player = handler->GetSession()->GetPlayer();
        Player* playerTarget = handler->getSelectedPlayer();
        if (!playerTarget)
            playerTarget = player;

        sLog->outDebug(LOG_FILTER_GENERAL, handler->GetTrinityString(LANG_ADDITEM), itemId, count);

        ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(itemId);
        if (!itemTemplate)
        {
            handler->PSendSysMessage(LANG_COMMAND_ITEMIDINVALID, itemId);
            handler->SetSentErrorMessage(true);
            return false;
        }

        // Restrict adding items
        if (count > 0)
            return false;

        // Subtract
        if (count < 0 || count == 1)
        {
            playerTarget->DestroyItemCount(itemId, -count, true, false);
            handler->PSendSysMessage(LANG_REMOVEITEM, itemId, -count, handler->GetNameLink(playerTarget).c_str());
            return true;
        }


        playerTarget->SendChatMessage("|cffB400B4A Gamemaster has just removed an event reward from your inventory.");
        handler->PSendSysMessage("|cffB400B4You have just removed an event prize. This command has been logged for future references.");
        return true;
    }
};

void AddSC_eventcommands()
{
    new eventcommands();
}