/*
* Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
* Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Common.h"
#include "DBCEnums.h"
#include "ObjectMgr.h"
#include "ArenaTeamMgr.h"
#include "GuildMgr.h"
#include "World.h"
#include "WorldPacket.h"
#include "DatabaseEnv.h"
#include "AchievementMgr.h"
#include "ArenaTeam.h"
#include "CellImpl.h"
#include "GameEventMgr.h"
#include "GridNotifiersImpl.h"
#include "Guild.h"
#include "Language.h"
#include "Player.h"
#include "SpellMgr.h"
#include "DisableMgr.h"
#include "ScriptMgr.h"
#include "MapManager.h"
#include "Battleground.h"
#include "BattlegroundAB.h"
#include "Map.h"
#include "InstanceScript.h"
#include "Group.h"
#include "BattlePetMgr.h"

BattlePetMgr::BattlePetMgr(Player* owner) : m_player(owner)
{
}

void BattlePetMgr::GetBattlePetList(PetBattleDataList &battlePetList) const
{
    auto spellMap = m_player->GetSpellMap();
    for (auto itr : spellMap)
    {
        if (itr.second->state == PLAYERSPELL_REMOVED)
            continue;

        if (!itr.second->active || itr.second->disabled)
            continue;

        SpellInfo const* spell = sSpellMgr->GetSpellInfo(itr.first);
        if (!spell)
            continue;

        // Is summon pet spell
        if ((spell->Effects[0].Effect == SPELL_EFFECT_SUMMON && spell->Effects[0].MiscValueB == 3221) == 0)
            continue;

        const CreatureTemplate* creature = sObjectMgr->GetCreatureTemplate(spell->Effects[0].MiscValue);
        if (!creature)
            continue;

        const BattlePetSpeciesEntry* species = sBattlePetSpeciesStore.LookupEntry(creature->Entry);
        if (!species)
            continue;

        PetBattleData pet(creature->Entry, creature->Modelid1, species->ID, spell->Id);
        battlePetList.push_back(pet);
    }
}

void BattlePetMgr::BuildBattlePetJournal(WorldPacket *data)
{
    PetBattleDataList petList;

    data->Initialize(SMSG_BATTLEPET_JOURNAL);
    *data << uint16(0); // unk
    data->WriteBit(1); // unk
    data->WriteBits(1, 20); // unk counter, may be related to battle pet slot
    data->WriteBit(1);

    GetBattlePetList(petList);
    data->WriteBits(petList.size(), 19);

    // bits part
    for (auto pet : petList)
    {
        data->WriteBit(true); // hasBreed, inverse
        data->WriteBit(true); // hasQuality, inverse
        data->WriteBit(true); // hasUnk, inverse
        data->WriteBits(0, 7); // name lenght
        data->WriteBit(false); // unk bit
        data->WriteBit(false); // has guid
    }

    // data part
    for (auto pet : petList)
    {
        *data << uint32(pet.m_displayID);
        *data << uint32(pet.m_summonSpellID); // Pet Entry
        *data << uint16(0); // xp
        *data << uint32(1); // health
        *data << uint16(1); // level
        // name
        *data << uint32(1); // speed
        *data << uint32(1); // max health
        *data << uint32(pet.m_entry); // Creature ID
        *data << uint32(1); // power
        *data << uint32(pet.m_speciesID); // species
    }

    *data << uint32(1);
    *data << uint32(1);
    *data << uint32(1);
    *data << uint32(1);
    *data << uint32(1);
    *data << uint8(1);

}

void BattlePetMgr::UpdateBattlePets(WorldPacket &data)
{
    
    PetBattleDataList petList;

    data.Initialize(SMSG_BATTLEPET_UPDATES);

    GetBattlePetList(petList);
    data.WriteBits(petList.size(), 19);

    for (PetBattleDataList::const_iterator pet = petList.begin(); pet != petList.end(); pet)
    {
        data.WriteBit(true); //invers
        data.WriteBit(true); //invers
        data.WriteBits(false, 7);

        data.WriteBit(false); // has guid

//        if (bits104[i])
//        {
//            guid[i] = new byte[8];

//            guid[i][6] = packet.ReadBit();
//            guid[i][7] = packet.ReadBit();
//            guid[i][4] = packet.ReadBit();
//            guid[i][3] = packet.ReadBit();
//            guid[i][1] = packet.ReadBit();
//            guid[i][2] = packet.ReadBit();
//            guid[i][0] = packet.ReadBit();
//            guid[i][5] = packet.ReadBit();
//        }

        data.WriteBit(true); //invers
    }

    data.WriteBit(true);

    for (PetBattleDataList::const_iterator pet = petList.begin(); pet != petList.end(); pet)
    {
//        if (bits104[i])
//        {
//            packet.ReadInt32("Unk Int32 120");
//            packet.ReadXORByte(guid[i], 1);
//            packet.ReadXORByte(guid[i], 3);
//            packet.ReadXORByte(guid[i], 5);
//            packet.ReadXORByte(guid[i], 7);
//            packet.ReadXORByte(guid[i], 2);
//            packet.ReadXORByte(guid[i], 6);
//            packet.ReadXORByte(guid[i], 0);
//            packet.ReadXORByte(guid[i], 4);

//            packet.WriteGuid("Guid", guid[i], i);
//        }


        data << uint32((*pet).m_displayID);
        data << uint32((*pet).m_summonSpellID);

//        if (bits129[i])
//            packet.ReadByte("Unk Int8 129", i);

        data << uint32((*pet).m_entry);
        data << uint32(1);
        data << uint32((*pet).m_speciesID);

        //name

//        if (bits22[i])
//            packet.ReadInt16("Unk Int16 22", i);

        data << uint16(2);
        data << uint32(3);
        data << uint32(4);
        data << uint32(5);
        data << uint16(6);

//        if (bits16[i])
//            packet.ReadInt16("Unk Int16 16", i);
    }

}

void WorldSession::HandleSummonBattlePet(WorldPacket& recvData)
{
    uint32 spellID = 0;
    recvData >> spellID;

    if (!_player->HasSpell(spellID))
        return;

    _player->CastSpell(_player, spellID, true);

    
    Creature* passenger = NULL;
    JadeCore::AllCreaturesOfEntryInRange check(_player, 7395, 20.0f);
    JadeCore::CreatureSearcher<JadeCore::AllCreaturesOfEntryInRange> searcher(_player, passenger, check);
    _player->VisitNearbyObject(30.0f, searcher);
    if (passenger)
    {
        _player->SetUInt32Value(PLAYER_FIELD_SUMMONED_BATTLE_PET_ID, passenger->GetGUIDLow());
        _player->SetUInt32Value(UNIT_FIELD_BATTLE_PET_COMPANION_ID, passenger->GetGUIDLow());
        _player->SetUInt32Value(UNIT_FIELD_BATTLE_PET_COMPANION_NAME_TIMESTAMP, uint32(time(NULL)));

        _player->SetCritterGUID(passenger->GetGUID());
        passenger->SetUInt64Value(UNIT_FIELD_CREATEDBY, _player->GetGUID());
        passenger->SetUInt64Value(UNIT_FIELD_WILD_BATTLE_PET_LEVEL, 1); 

//        WorldPacket packet;
//        packet << passenger->GetEntry();
//        packet << passenger->GetGUID();
//        HandleCreatureQueryOpcode(packet);

    }

}
