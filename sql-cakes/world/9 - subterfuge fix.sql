INSERT INTO `world`.`spell_script_names` (`spell_id`, `ScriptName`) VALUES (115191, 'spell_rog_subterfuge');
INSERT INTO `world`.`spell_script_names` (`spell_id`, `ScriptName`) VALUES (115192, 'spell_rog_subterfuge_buff');
UPDATE `world`.`spell_script_names` SET `spell_id`=1784 WHERE  `spell_id`=115191 AND `ScriptName`='spell_rog_subterfuge';
INSERT INTO `world`.`spell_linked_spell` (`spell_trigger`, `spell_effect`, `comment`) VALUES (115191, 1784, 'Subterfuge stealth trigger stealth');